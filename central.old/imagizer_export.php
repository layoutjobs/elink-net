<?php
$qstring=split("&",$_SERVER['QUERY_STRING']);
$acao=$qstring[0];

$variaveis=explode(",",$acao);

include("classes/Suporte.php");
include("framework/class_imagizer.php");

settype($variaveis[1],"integer");
settype($variaveis[2],"integer");
settype($variaveis[3],"integer");
if($variaveis[4]=="1" or $variaveis[4]=="0") { settype($variaveis[4],"boolean"); } else { settype($variaveis[4],"integer"); }
if($variaveis[5]=="1" or $variaveis[5]=="0") { settype($variaveis[5],"boolean"); }
$variaveis[5]=true;
//settype($variaveis[8],"string");

//imagizer($imagem,$dw,$dh,$formato=1,$curvas=false,$saida=true,$sextensao="",$mascara="",$mascara_pos=1)
//             0     1  2       3            4           5            6           7             8


$tmp_dir="temp/";
$tmp_name=Suporte::nomear($acao);
$tmp_name=str_replace(".","_",$tmp_name).".{$variaveis[6]}";
$tmp_url="{$tmp_dir}{$tmp_name}";

$t_msksize=($variaveis[7]!="" AND file_exists($variaveis[7]))?filesize($variaveis[7]):0;

//imagizer($imagem,$dw,$dh,$formato=1,$curvas=false,$saida=true,$sextensao="",$mascara="",$mascara_pos=1)
//             0     1  2       3            4           5            6           7             8

header("Pragma: public");
header("Content-type: image/jpg");
header("Content-Disposition: inline; filename=\"{$tmp_name}\"");
header("Content-Transfer-Encoding: binary");

if(file_exists($tmp_url) AND filesize($tmp_url)>(1000+$t_msksize))
{
    exit(header("Location: {$tmp_url}"));
}
else
{
    if(is_dir($tmp_dir) AND is_writable($tmp_dir))
    {
        $variaveis[5]=$tmp_url;
    }
	imagizer($variaveis[0],$variaveis[1],$variaveis[2],$variaveis[3],$variaveis[4],$variaveis[5],$variaveis[6],$variaveis[7],$variaveis[8]);
	//imagizer2($variaveis[0],$variaveis[1],$variaveis[2],$variaveis[3],$variaveis[4],$variaveis[5],$variaveis[6],$variaveis[7],$variaveis[8]);
    if(is_dir($tmp_dir) AND is_writable($tmp_dir))
    {
    	exit(header("Location: {$tmp_url}"));
   	}
}
?>