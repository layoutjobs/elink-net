<div class="d_botoes">
	
    <?
    if(Central::permissao("dados"))
	{
		?>
		<a class="button" href="usuario/dados">
		<img src="imagens/64x64/user.png"/>
		<span>Dados do Usu�rio</span>
		</a>
		<?
	}
    if(Central::permissao("extrato"))
	{
		?>
		<a class="button" href="usuario/extrato">
		<img src="imagens/64x64/calculator.png"/>
		<span>Extrato Financeiro</span>
		</a>
		<?
	}
    if(Central::permissao("suporte"))
	{
		?>
		<a class="button" href="usuario/suporte">
		<img src="imagens/64x64/mail_write.png"/>
		<span>Suporte</span>
		</a>
		<?
	}
    if(Central::permissao("acesso"))
	{
		?>
		<a class="button" href="usuario/acesso">
		<img src="imagens/64x64/calendar.png"/>
		<span>Relat�rio de Acessos</span>
		</a>
		<?
	}
    if(Central::permissao("chat"))
	{
		?>
		<a class="button" href="usuario/chat">
		<img src="imagens/64x64/message.png"/>
		<span>Atendimento Chat</span>
		</a>
		<?
	}
    ?>

</div>

<div style="padding: 0 10px;">
	
	<?
	
	if(Central::permissao("extrato"))
	{
		include("index_extrato.php");
		?>
		<br />
		<?
	}
	
	if(Central::permissao("recados"))
	{
		$query_recados = "SELECT * FROM central_recados WHERE para='{$_SESSION["usuario"]["login"]}' OR para='todos' ORDER BY id DESC";
		$r_recados = mysql_query($query_recados, $vigo);
		if(mysql_num_rows($r_recados)>0)
		{
			include("recados.php");
			?>
			<br />
			<?
		}
	}
	
	if(Central::permissao("acesso"))
	{
		?>
		
		<h1>
			<a href="usuario/acesso_grafico">
			Acessos
			</a>
		</h1>
		
		<?
		
		$mikrotik=Conexao::conn("mikrotik");
		
		$r_login = mysql_query("SELECT DISTINCT username FROM radcheck WHERE numero='{$_SESSION["usuario"]["numero"]}' AND username NOT LIKE '%:%:%:%:%:%' ORDER BY username", $mikrotik);
				
		//$dt_inicio=($dt_inicio=="")?date("d/m/Y",strtotime("-30 day")):$dt_inicio;
		//$dt_final=($dt_final=="")?date("d/m/Y"):$dt_final;
		$dt_inicio=date("01/m/Y");
		$dt_final=date("t/m/Y");
		
		for($i=0;$i<mysql_num_rows($r_login);$i++)
		{
			$d_login=mysql_fetch_array($r_login);
			?>
			<div id="d_grafico_<?=$i?>" style="padding: 20px 0; text-align: center;">
			    <script type="text/javascript">
			    <?
			    $a_grafico=array();
			    $a_grafico["login"]=$d_login["username"];
			    $a_grafico["dt_inicio"]=$dt_inicio;
			    $a_grafico["dt_final"]=$dt_final;
			    $t_dados=base64_encode(json_encode($a_grafico));
			    ?>
			    t_width=$("#d_grafico_<?=$i?>").width();
			    //var chart = new FusionCharts("framework/fusioncharts/FCF_Line.swf", "ChartId",t_width,"500");
			    var chart = new FusionCharts("framework/fusioncharts/FCF_MSArea2D.swf", "ChartId",t_width,"300");
			    chart.setDataURL("usuario/acesso_grafico/?acao=dados%26dados="+escape("<?=$t_dados?>"));
			    chart.setTransparent();		   
			    chart.render("d_grafico_<?=$i?>");
			    </script>
			</div>			
			<?
		}
		
	}
	?>

</div>