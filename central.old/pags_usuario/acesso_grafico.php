<?

// Lista o extrato de horas de conexoes via radius

global $mikrotik;
$mikrotik=Conexao::conn("mikrotik");

if($acao=="dados")
{
	ob_clean();
	
	header("Content-type: application/xml; charset=utf-8");
	
	$a_cores=array("5901FC","FC0101");
	
	$a_dados=json_decode( base64_decode($dados) ,true);
	extract($a_dados);
	
	$inicio=Suporte::datar($dt_inicio);
	$fim=Suporte::datar($dt_final);
	
	// Seleciona a lista de conexoes de radacct
	//$r_horas = mysql_query("SELECT * FROM radacct WHERE (UserName='$login') AND (AcctStartTime>='$inicio') AND (AcctStopTime<='$fim') AND (AcctStopTime<>'00/00/0000 00:00:00') ORDER by RadAcctId ASC",$mikrotik);
	
	//var_dump(Suporte::diasDiferenca($inicio,$fim));
	$login=Suporte::protege($login);
	$r_username = mysql_query("SELECT DISTINCT username FROM radcheck WHERE numero='{$_SESSION["usuario"]["numero"]}' AND username='{$login}'", $mikrotik);
	if(mysql_num_rows($r_username)==0)
	{
		exit("N�o encontrado.");
	}
	
	?>
	<graph caption='Total de tr�fego - <?=$login?>' yAxisName='Megabytes' divlinecolor='F47E00' numdivlines='4' showAreaBorder='1' areaBorderColor='000000' numberPrefix='' numberSuffix=' Mb' showNames='1' numVDivLines='29' vDivLineAlpha='30' yAxisMaxValue='100' formatNumberScale='0' rotateNames='1' decimalPrecision='2'>
		
		<categories>
		<?
		for($i=0;$i<=Suporte::diasDiferenca($inicio,$fim);$i++)
		{
			$data=date("d/m/Y ",strtotime($inicio." +{$i} day"));
			$data.=substr($a_semana[date("l",strtotime($inicio." +{$i} day"))],0,3);
			?>
			<category name='<?=$data?>'/>
			<?
		}
		?>
		</categories>
		
	   	<dataset seriesname='Upload' color='FF5904' showValues='0' areaAlpha='50' showAreaBorder='1' areaBorderThickness='2' areaBorderColor='FF0000'>
		<?
		for($i=0;$i<=Suporte::diasDiferenca($inicio,$fim);$i++)
		{
			$data=date("Y-m-d",strtotime($inicio." +{$i} day"));
			$r_horas = mysql_query("SELECT SUM(AcctInputOctets) FROM radacct WHERE (UserName='$login') AND (AcctStartTime LIKE '{$data}%') AND (AcctStopTime<>'00/00/0000 00:00:00') ORDER by RadAcctId ASC",$mikrotik);
			?>
			<set value='<?=floatval(mysql_result($r_horas,0,0)/1024/1024)?>'/>
			<?
		}
		?>
		</dataset>
		
		<dataset seriesname='Download' color='99cc99' showValues='0' areaAlpha='50' showAreaBorder='1' areaBorderThickness='2' areaBorderColor='006600'>
		<?
		for($i=0;$i<Suporte::diasDiferenca($inicio,$fim);$i++)
		{
			$data=date("Y-m-d",strtotime($inicio." +{$i} day"));
			$r_horas = mysql_query("SELECT SUM(AcctOutputOctets) FROM radacct WHERE (UserName='$login') AND (AcctStartTime LIKE '{$data}%') AND (AcctStopTime<>'00/00/0000 00:00:00') ORDER by RadAcctId ASC",$mikrotik);
			?>
			<set value='<?=floatval(mysql_result($r_horas,0,0)/1024/1024)?>'/>
			<?
		}
		?>
		</dataset>

	</graph>
	<?
	exit();
}

$mk_login = mysql_query("SELECT DISTINCT username FROM radcheck WHERE numero='{$_SESSION["usuario"]["numero"]}' AND username NOT LIKE '%:%:%:%:%:%' ORDER BY username", $mikrotik);
$totalRows_mk_login = mysql_num_rows($mk_login);

$dt_inicio=($dt_inicio=="")?date("d/m/Y",strtotime("-30 day")):$dt_inicio;
$dt_final=($dt_final=="")?date("d/m/Y"):$dt_final;

?>
<h1>Gr�fico de consumo</h1>

<p>
<b>Aviso Legal</b><br />
Esse extrato &eacute; para consulta particular do cliente e a utiliza&ccedil;&atilde;o do
mesmo   para qualquer outra finalidade ser&aacute; de responsabilidade exclusiva do cliente.
Caso voc&ecirc; verifique a utiliza&ccedil;&atilde;o indevida de sua senha, solicitamos que
voc&ecirc;   providencie a altera&ccedil;&atilde;o   imediata da mesma ou entre em contato com o
Provedor de Acesso.
</p>

<form class="f_filtro" action="" method="post">
<input type="hidden" name="acao" value="listar"/>
    
	<span>
	<label>Login de acesso</label>
	<select name="login" id="login">
	<?php
	while ($row_mk_login = mysql_fetch_assoc($mk_login))
	{
		print "<option value='".$row_mk_login['username']."'>".$row_mk_login['username']."</option>";
	}
	?>
	</select>		
	</span>
	
	<span>
	<label>Inicio</label>
	<input class="c_data" name="dt_inicio" type="text" class="myinputstyle" style="myinputstyle" value="<?=$dt_inicio?>" size="20"/>
    </span>
	
	<span>
	<label>Fim</label>
	<input class="c_data" name="dt_final" type="text" class="myinputstyle" value="<?=$dt_final?>" size="20"/>
	</span>
    
    <span>
	<button type="submit">
	Carregar Gr�fico</button>
	</span>
    
</form>

<?
if($acao=="listar")
{
	?>
	<div id="d_grafico" style="text-align: center;">
	    <script type="text/javascript">
	    <?
	    $a_grafico=array();
	    $a_grafico["login"]=$login;
	    $a_grafico["dt_inicio"]=$dt_inicio;
	    $a_grafico["dt_final"]=$dt_final;
	    $t_dados=base64_encode(json_encode($a_grafico));
	    ?>
	    t_width=$("#d_grafico").width();
	    //var chart = new FusionCharts("framework/fusioncharts/FCF_Line.swf", "ChartId",t_width,"500");
	    var chart = new FusionCharts("framework/fusioncharts/FCF_MSArea2D.swf", "ChartId",t_width,"500");
	    chart.setDataURL("usuario/acesso_grafico/?acao=dados%26dados="+escape("<?=$t_dados?>"));
	    chart.setTransparent();		   
	    chart.render("d_grafico");
	    </script>
	</div>
	<?
}
?>
