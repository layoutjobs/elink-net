<?php
require 'mod_2via.php';

function MensagemErro() {
   print "<html><title>VIGOprovider</title><font face=Verdana size=+1>ERRO !!!<br>";
   print "N&atilde;o existem boletos em aberto ou<br>";
   print "N&atilde;o foi selecionado nenhum boleto da lista anterior<br><br>";
   print "Para maiores informa&ccedil;&otilde;es contate o WEBMASTER.<br>";
   print "<br><a href='javascript:history.go(-1);'>Clique aqui para retornar</a></font></html>";
   exit;
}

// ==================================
// Configuracoes

$gera_segunda_via = TRUE;
$remove_instrucoes_apos_vcto = FALSE;

// Pega os valores de multa e juros a partir do banco de dados
$query = mysql_query('SELECT multa, juros FROM central', $vigo) or die(mysql_error());
$row = mysql_fetch_assoc($query);
//$multa = (double) $row['multa'];
//$mora = (double) $row['juros'];
$multa=2;
$juros=2/100;
$mora=0.19/100;

//$juros = $multa/100;
//$mora  = $mora/100;

// Se o boleto for impresso pela central, conceder desconto de:
$Desconto = 0;

// ==================================

$LstBancos = array(
   '001' => '001-9',
   '104' => '104-0',
   '356' => '356-5',
   '409' => '409-0',
   '341' => '341-7',
   '237' => '237-2',
   '099' => '099',
   '748' => '748-X',
   '399' => '399-9',
   '003' => '003-5',
   '004' => '004-3',
   '756' => '756-0',
   '041' => '041-8'
);

header("Content-type: text/html\n\n");

if ( !isset($_GET["NnR"] ) ) MensagemErro(); // Erro, parametro nosso numero nao informado

// Conecta no Banco de dados
//require_once('Connections/vigo.php');

$Nnumero = mysql_real_escape_string($_GET["NnR"]);
//$IdCliente = $_GET["nId"];
$IdCliente = $_SESSION["usuario"]["numero"];

// Pega os dados do boleto
//mysql_select_db($database_vigo, $vigo);
$query = mysql_query("SELECT * FROM boletos WHERE nnumero='$Nnumero' and numero='$IdCliente'") or die(mysql_error());
$boleto = mysql_fetch_assoc($query);

if ( mysql_num_rows($query) < 1 ) MensagemErro(); // Erro, nenhum boleto encontrado

mysql_free_result($query);

// Informacoes do banco
$IdBanco = $boleto['idbanco'];
$NumeroBanco = substr($boleto['nboleto2'], 0, 3);
$NumeroBancoI = $LstBancos[$NumeroBanco];
$IdEmpresa = $boleto['idempresa'];

// Informacoes do Boleto
$NossoNumero = $boleto['nnumero'];
$CodigoBarras = $boleto['nboleto1'];
$LinhaDigitavel = $boleto['nboleto2'];
$Vencimento = date('d/m/Y', strtotime($boleto['vcto']));
$DataProcessamento = date('d/m/Y', strtotime($boleto['emissao']));
$Data = date('d/m/Y');

/*
  Se o boleto for impresso depois do vencimento,
  retirar as instrucoes de desconto
*/
if ($remove_instrucoes_apos_vcto) {
   if (strtotime($boleto['vcto'])<strtotime(date('Y-m-d')))
      $Instrucoes = '';
   else
      $Instrucoes = $boleto['obs'];
} else {
   $Instrucoes = $boleto['obs'];
}

$Valor = sprintf("%10.2f", $boleto['valor']);
$Juros = '';
$ValorCalculado = $Valor;

// Informacoes do Sacado
$NomeSacado = $boleto['nome'];
$Endereco = $boleto['endereco'];
$Cep = $boleto['cep'];
$Cidade = $boleto['cidade'];

// Pega as informacoes do Cedente
$query = mysql_query("SELECT rsocial, cnpj FROM empresas WHERE id=$IdEmpresa") or die(mysql_error());
$empresa = mysql_fetch_assoc($query);
$Cedente = $empresa['rsocial'] . " - CNPJ: " . $empresa['cnpj'];
mysql_free_result($query);

// Pega as informacoes do Banco
$query = mysql_query("SELECT agencia, conta, convenio FROM bancos WHERE id='$IdBanco'") or die(mysql_error());
$banco = mysql_fetch_assoc($query);
$CONTA_DA_EMPRESA = $banco['agencia'] . " / " . $banco['conta'];
$Carteira = $banco['convenio'];
mysql_free_result($query);

if ($Desconto>0) {
   $Valor -= $Desconto;
   $Valor = sprintf('%10.2f', $Valor);
   $ValorCalculado = $Valor;
}
$Desconto = sprintf('%10.2f', $Desconto);

$dias_em_atraso = DiasEmAtraso($boleto['vcto']);
if( ($dias_em_atraso>0) and ($gera_segunda_via) ) {
   $multa = $Valor*$juros;
   $mora  = ($Valor*$mora) * $dias_em_atraso;
   $Juros = $multa+$mora;
   
   $ValorCalculado = $Valor + $Juros;
   $Juros = sprintf("%10.2f", $Juros);
   $ValorCalculado = sprintf("%10.2f", $ValorCalculado);
   
   $Vencimento = NovoVencimento();
}

// RE-gera o codigo de barras para sair com o $Desconto
$dados_boleto = array(
   'numbanco' => $NumeroBanco,
   'agencia'  => $banco['agencia'],
   'conta'    => $banco['conta'],
   'nnumero'  => $NossoNumero,
   'convenio' => $banco['convenio'],
   'codigoescritural' => $banco['codigoescritural'],
   'valor'    => $ValorCalculado,
   'vencimento' => $Vencimento
);

/* 
   Sempre atualizar os dados do boleto
   - Clientes com boletos importados podem ter o nboleto1 e nboleto2
   zerados que o sistema ir� atualiza-los desta forma.
   - Se for informado um $Desconto, o sistema ter� que recalcular o 
   boleto (cod barras e linha digit�vel)
 */
$NovosDados = NovosDadosBoleto($dados_boleto);
if(!is_null($NovosDados)) {
   $CodigoBarras   = $NovosDados['codigobarras'];
   $LinhaDigitavel = $NovosDados['linhadigitavel'];
}

?>
<script type="text/javascript">
$(document).ready(function(){  
	window.print();  
});
</script>

<style type="text/css">
* { font-family: Verdana,Tahoma,Arial,Helvetica,sans-serif; font-size: 10px; }
body { margin: 10px 0 0 10px; background: #ffffff; }
table { border: 1px solid gray; border-collapse: collapse; padding: 0; width: 610px; position: relative;}
table td { vertical-align: top; text-align: center;	height: 25px; border: 1px solid; padding-top: 1px; }
table td#logo { border-right: 0; }
table td#Titulo { font-size: 18px; text-align: right; padding-top: 5px; padding-right: 15px; border-left: 0; }
table td#Num { padding-top: 8px; font-size: 14px; font-weight: bolder;}
table td#Numero { font-family:Helvetica; font-size:1.3em; font-weight:bold; padding-top:10px;}
table td.Instru { padding: 10px 0 0 10px; text-align: left;}
table td.Rodape { font-weight: bolder; text-align: left; padding: 15px 0 0 15px; height: 70px; background:#e1e1e1; }
table td.esp { text-align: left; padding: 1px 0 0 5px; }
table td.Dir { text-align: left; padding: 1px 0 0 5px; width: 150px; }
table td.DirEsp { text-align: left; padding: 1px 0 0 5px; width: 150px;	background:#e1e1e1;	}
table td.DirEsp2 { text-align: right; padding: 1px 5px 0 0; width: 150px; background:#e1e1e1; }
table td.Texto1 { padding-left: 5px; text-align: left; }
div#Auten { width: 610px; position: static; text-align: right; padding: 5px 0 40px 0; border-bottom: 1px dotted gray; margin-bottom: 10px; }
</style>

<table>
  <tbody>
     <tr>
        <td id="logo"><img src="boletos/banco-<?= $NumeroBanco ?>.bmp"></td>
        <td colspan="5" id="Titulo">RECIBO DO SACADO</td>
     </tr>
     <tr>
        <td colspan="5" class="Texto1">Cedente<br>&nbsp;&nbsp;<b><?= $Cedente ?></b></td>
        <td class="DirEsp2">Vencimento<br><b><?= $Vencimento ?></b></td>
     </tr>
     <tr>
        <td>Data<br><b><?= $Data ?></b></td>
        <td>Documento</td>
        <td>Esp&eacute;cie<br><b>DS</b></td>
        <td>Aceite<br><b>N</b></td>
        <td>Dt.Processamento<br><b><?= $DataProcessamento ?></b></td>
        <td class="Dir">Nosso N&uacute;mero<br>&nbsp;&nbsp;<b><?= $NossoNumero ?></b></td>
     </tr>
     <tr>
        <td>Conta</td>
        <td>Carteira<br><b><?= $Carteira ?></b></td>
        <td>Esp&eacute;cie<br><b>Real</b></td>
        <td>Quantidade<br></td>
        <td>Valor</td>
        <td class="DirEsp">(=) Valor do documento<br>&nbsp;&nbsp;<b><?= $Valor ?></b></td> 
     </tr>
     <tr>
        <td rowspan="5" colspan="5" class="Instru">
           <b>INSTRU&Ccedil;&Otilde;ES:<br></b>*** VALORES EM REAIS ***<br><br><pre><?= $Instrucoes ?></pre>
        </td>
        <td class="Dir">(-) Desconto<br>&nbsp;&nbsp;<?= $Desconto ?></td>
     </tr>
     <tr><td class="Dir">(-) Outras dedu&ccedil;&otilde;es<br>&nbsp;&nbsp;0.00</td></tr>
     <tr><td class="Dir">(+) Mora / Multa / Juros<br>&nbsp;&nbsp;<b><?= $Juros ?></b></td></tr>
     <tr><td class="Dir">(+) Outros acr&eacute;scimos<br>&nbsp;&nbsp;0.00</td></tr>
     <tr><td class="Dir">(=) Valor cobrado<br>&nbsp;&nbsp;<b><?= $ValorCalculado ?></b></td></tr>
     <tr>
        <td colspan="6" class="Rodape">
           <?= $NomeSacado ?><br>
           <?= $Endereco ?><br>
           <?= $Cep ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
           <?= $Cidade ?>
        </td>
     </tr>
  </tbody>
</table>

<div id="Auten">Autentica&ccedil;&atilde;o Mec&acirc;nica</div>
<table>
  <tbody>
     <tr>
        <td><img src="boletos/banco-<?= $NumeroBanco ?>.bmp"></td>
        <td id="Num"><?= $NumeroBancoI ?></td>
        <td colspan="4" id="Numero"><?= $LinhaDigitavel ?></td>
     </tr>
     <tr>
        <td colspan="5" class="Texto1">
           Local de pagamento<br>&nbsp;&nbsp;
           <b>QUALQUER AG&Ecirc;NCIA BANC&Aacute;RIA AT&Eacute; O VENCIMENTO</b>
        </td>
        <td class="DirEsp2">Vencimento<br><b><?= $Vencimento ?></b></td>
     </tr>
     <tr>
        <td colspan="5" class="Texto1">Cedente<br>&nbsp;&nbsp;<b><?= $Cedente ?></b></td>
        <td class="Dir">Ag&ecirc;ncia / C&oacute;digo<br>&nbsp;&nbsp;<?= $CONTA_DA_EMPRESA ?></td>
     </tr>
     <tr>
        <td>Data<br><b><?= $Data ?></b></td>
        <td>Documento</td>
        <td>Esp&eacute;cie<br><b>DS</b></td>
        <td>Aceite<br><b>N</b></td>
        <td>Dt.Processamento<br><b><?= $DataProcessamento ?></b></td>
        <td class="Dir">Nosso N&uacute;mero<br>&nbsp;&nbsp;<?= $NossoNumero ?></td>
     </tr>
     <tr>
        <td>Conta</td>
        <td>Carteira<br><b><?= $Carteira ?></b></td>
        <td>Esp&eacute;cie<br><b>Real</b></td>
        <td>Quantidade<br></td>
        <td>Valor</td>
        <td class="DirEsp">(=) Valor do documento<br>&nbsp;&nbsp;<b><?= $Valor ?></b></td>
     </tr>
     <tr>
        <td rowspan="5" colspan="5" class="Instru">
           <b>INSTRU&Ccedil;&Otilde;ES:</b><br>*** VALORES EM REAIS ***<br><br>
           <pre><?= $Instrucoes ?></pre>
        </td>
        <td class="Dir">(-) Desconto<br>&nbsp;&nbsp;<?= $Desconto ?></td>
     </tr>
     <tr><td class="Dir">(-) Outras dedu&ccedil;&otilde;es<br>&nbsp;&nbsp;0.00</td></tr>
     <tr><td class="Dir">(+) Mora / Multa / Juros<br>&nbsp;&nbsp;<b><?= $Juros ?></b></td></tr>
     <tr><td class="Dir">(+) Outros acr&eacute;scimos<br></td></tr>
     <tr><td class="Dir">(=) Valor cobrado<br>&nbsp;&nbsp;<b><?= $ValorCalculado ?></b></td></tr>
     <tr>
        <td colspan="6" class="Rodape">
           <?= $NomeSacado ?><br>
           <?= $Endereco ?><br>
           <?= $Cep ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $Cidade ?>
        </td>
     </tr>
  </tbody>
</table>

<div style="width: 610px;text-align:left;padding-top:5px;">
  <img src="usuario/codbarra/?numero=<?= $CodigoBarras ?>">
</div>
