<h1>
	<a href="usuario/extrato">
	Extrato Boletos
	</a>
</h1>

<?php

$query_boleto = "SELECT * FROM boletos WHERE numero = '{$_SESSION["usuario"]["numero"]}' ORDER BY emissao DESC LIMIT 5";
$r_boleto = mysql_query($query_boleto, $vigo) or die(mysql_error());
$row_boleto = mysql_fetch_assoc($r_boleto);

if(mysql_num_rows($r_boleto)>0)
{
	?>
	<table class="t_listagem" style="width: 100%;" cellpadding="5" cellspacing="0">
	
	<thead>
	<tr>
		<td></td>
		<td>Data</td>
		<td>N&uacute;mero do T&iacute;tulo</td>
		<td>Valor do boleto</td>
		<td>Vencimento</td>
		<td>Detalhes</td>
	</tr>
	</thead>
	
	<tbody>
	   
	<?php
	do
	{
		?>
		<tr>
			
			<td style="width: 1%;">
			<img src="imagens/icones/<?=($row_boleto['vpago']=='0')?"flag.gif":"apply.gif"?>"/>
			</td>
			
			<td>
			<?php $dia = date("d/m/Y",strtotime($row_boleto['vcto'])); echo $dia; ?>
			</td>
			
			<td>
			<?php echo $row_boleto['nnumero']; ?>
			</td>
			
			<td>
			<!--
			R$ <?php echo sprintf("%10.2f", $row_boleto['valor']); ?>
			-->
			<?php
			if ($row_boleto['pago']=='0')
			{
				echo 'R$ ', Financeiro::juros($row_boleto['valor'], $row_boleto['vcto']);
			} else {
				echo 'R$ ', sprintf("%10.2f", $row_boleto['valor']);
			}
			?>				
			</td>
			
			<td>
			<?php
			$dtFinal = ( ( $row_boleto['pago'] != "1" ) ? date('Y-m-d') : $row_boleto['dt_pag'] );
			$atraso = Suporte::datar_passado(strtotime($row_boleto['vcto']),strtotime($dtFinal));
			echo ( ($row_boleto['vpago']=='0') ? $atraso : "Pago");
			?>
			</td>
			
			<td>
				
				<a class="button lytediv" href="#info_<?php echo $row_boleto['nnumero']; ?>">
				Detalhes</a>
				
				<?
				if($row_boleto['pago']=='0' AND Central::permissao("2via"))
				{
					?>
					<a class="button" href="usuario/<?=$ModeloBoleto?>/?box=sim&NnR=<?= $row_boleto['nnumero'] ?>&nId=<?= $_SESSION["usuario"]["numero"] ?>" target="_blank">
					<img class="mm" src="imagens/icones/fileprint.gif"/>
					Imprimir Boleto
					</a>					
					<?
				}
				?>				
					
				<!-- ESSA DIV CONTEM OS DADOS QUE IRAO APARECER AO SE PRESSIONAR O LINK i -->   
				
				<div id="info_<?php echo $row_boleto['nnumero']; ?>" style="display:none">
				<div class="f_cadastro f_cadastro_linha">
					
					<h3>
					Cliente:
					<?php echo $row_boleto['nome']; ?>
					</h3>
					
					<span>
					<label>N�mero do Titulo:</label>
					<?php echo $row_boleto['nnumero']; ?>
					</span>
					
					<span>
					<label>Valor:</label>
					R$ <?php echo sprintf("%10.2f", $row_boleto['valor']); ?>
					</span>
					
					<span>
					<label>Vencimento:</label>
					<?php echo $dia; ?>
					</span>
					
					<span>
					<label>Dias em atraso:</label>
					<?php
					$dtFinal = ( ( $row_boleto['pago'] != "1" ) ? date('Y-m-d') : $row_boleto['dt_pag'] );
					$atraso = Suporte::datar_passado(strtotime($row_boleto['vcto']),strtotime($dtFinal));
					echo ( ($row_boleto['vpago']=='0') ? $atraso : "Pago");
					?>
					</span>
					
					<span>
					<label>Data do pagamento:</label>
					<?=($row_boleto['pago']=="1")?date("d/m/Y",strtotime($row_boleto['dt_pag'])):"N�o pago"?>
					</span>
					
					<span>
					<label>Valor total:</label>
					<!--
					R$ <?php echo sprintf("%10.2f", $row_boleto['vpago']);?>
					-->
					<?php
					if ($row_boleto['pago']=='0')
					{
						echo 'R$ ', Financeiro::juros($row_boleto['valor'], $row_boleto['vcto']);
					} else {
						echo 'R$ ', sprintf("%10.2f", $row_boleto['valor']);
					}
					?>					
					</span>
				
				</div>
				</div>
				
				<!-- FIM DAS MENSAGENS -->			
				
			</td>
			 
		</tr>
		<?php
	}
	while ($row_boleto = mysql_fetch_assoc($r_boleto));
	?>
	</tbody>
	
	</table>

	<?php
}
else
{
	?>
  	<p>Cliente sem registro de boleto.</p>
	<?php
}
?>