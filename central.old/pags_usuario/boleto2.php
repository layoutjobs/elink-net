<?php

$Mostra_CodBarras = "S";
$Calcula_Juros = "S";

$Taxa_Juros = 2/100;    // 2%
$Taxa_Mora  = 0.03/100; // 0.03%

$LstBancos = array(
   '001' => '001-9',
   '104' => '104-0',
   '356' => '356-5',
   '409' => '409-0',
   '341' => '341-7',
   '237' => '237-2',
   '099' => '099',
   '748' => '748-X',
   '399' => '399-9',
   '003' => '003-5',
   '004' => '004-3',
   '756' => '756-0',
   '041' => '041-8'
);

header("Content-type: text/html\n\n");

if ( !isset($_GET["NnR"] ) ) MensagemErro(); // Erro, parametro nosso numero nao informado

// Conecta no Banco de dados
//require_once('Connections/vigo.php');

$Nnumero = $_GET["NnR"];
$IdCliente = $_GET["nId"];

// Pega os dados do boleto
//mysql_select_db($database_vigo, $vigo);
$query = mysql_query("SELECT * FROM boletos WHERE nnumero='$Nnumero' and numero='$IdCliente'") or die(mysql_error());
$boleto = mysql_fetch_assoc($query);

if ( mysql_num_rows($query) < 1 ) MensagemErro(); // Erro, nenhum boleto encontrado

mysql_free_result($query);

// Informacoes do banco
$IdBanco = $boleto['idbanco'];
$NumeroBanco = substr($boleto['nboleto2'], 0, 3);
$NumeroBancoI = $LstBancos[$NumeroBanco];
$IdEmpresa = $boleto['idempresa'];
$Carteira = '';

// Informacoes do Boleto
$NossoNumero = $boleto['nnumero'];
$CodigoBarras = $boleto['nboleto1'];
$LinhaDigitavel = $boleto['nboleto2'];
$Vencimento = date('d/m/Y', strtotime($boleto['vcto']));
$DataProcessamento = date('d/m/Y', strtotime($boleto['emissao']));
$Data = date('d/m/Y');
$Instrucoes = $boleto['obs'];
$Valor = sprintf("%10.2f", $boleto['valor']);
$Juros = '';
$ValorCalculado = '';

// Informacoes do Sacado
$NomeSacado = $boleto['nome'];
$Endereco = $boleto['endereco'];
$Cep = $boleto['cep'];
$Cidade = $boleto['cidade'];

// Pega as informacoes do Banco
$query = mysql_query("SELECT agencia, conta, cedente FROM bancos WHERE id='$IdBanco'") or die(mysql_error());
$banco = mysql_fetch_assoc($query);
$Cedente = $banco['cedente'];
$CONTA_DA_EMPRESA = $banco['agencia'] . " / " . $banco['conta'];
mysql_free_result($query);

// =======================================
// Calculo de Juros
$Vencido = DiasEmAtraso($Vencimento);

if (( $Vencido > 0 ) and ( $Calcula_Juros == 'S' )) {
   CalculaJuros();
}
// =======================================

print "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>";
print "<html><head>";
print "<meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>";
print "<title>Boleto</title>";
print "<style type='text/css'>";
print "* { font-family: Verdana,Tahoma,Arial,Helvetica,sans-serif; font-size: 10px; }";
print "body { margin: 10px 0 0 10px; background: #ffffff; }";
print "table { border: 1px solid gray; border-collapse: collapse; padding: 0; width: 610px; position: relative;}";
print "table td { vertical-align: top; text-align: center;	height: 25px; border: 1px solid; padding-top: 1px; }";
print "table td#logo { border-right: 0; }";
print "table td#Titulo { font-size: 18px; text-align: right; padding-top: 5px; padding-right: 15px; border-left: 0; }";
print "table td#Num { padding-top: 8px; font-size: 14px; font-weight: bolder;}";
print "table td#Numero { padding-top: 10px; font-size: 10px; font-weight: bolder;}";
print "table td.Instru { padding: 10px 0 0 10px; text-align: left;}";
print "table td.Rodape { font-weight: bolder; text-align: left; padding: 15px 0 0 15px; height: 70px; background:#e1e1e1; }";
print "table td.esp { text-align: left; padding: 1px 0 0 5px; }";
print "table td.Dir { text-align: left; padding: 1px 0 0 5px; width: 150px; }";
print "table td.DirEsp { text-align: left; padding: 1px 0 0 5px; width: 150px;	background:#e1e1e1;	}";
print "table td.DirEsp2 { text-align: right; padding: 1px 5px 0 0; width: 150px; background:#e1e1e1; }";
print "table td.Texto1 { padding-left: 5px; text-align: left; }";
print "div#Auten { width: 610px; position: static; text-align: right; padding: 5px 0 40px 0; border-bottom: 1px dotted gray; margin-bottom: 10px; }";
print "</style></head><body><table><tbody>";
print "<tr> <td id='logo'><img src='banco-$NumeroBanco.bmp'></td> <td colspan='5' id='Titulo'>RECIBO DO SACADO</td> </tr>";
print "<tr> <td colspan='5' class='Texto1'>Cedente<br>&nbsp;&nbsp;<b>$Cedente</b></td> <td class='DirEsp2'>Vencimento<br><b>$Vencimento</b></td> </tr>";
print "<tr> <td>Data<br><b>$Data</b></td> <td>Documento</td> <td>Esp&eacute;cie<br><b>DS</b></td> <td>Aceite<br><b>N</b></td> <td>Dt.Processamento<br><b>$DataProcessamento</b></td> <td class='Dir'>Nosso N&uacute;mero<br>&nbsp;&nbsp;<b>$NossoNumero</b></td> </tr>";
print "<tr> <td>Conta</td> <td>Carteira<br><b>$Carteira</b></td> <td>Esp&eacute;cie<br><b>Real</b></td> <td>Quantidade<br></td> <td>Valor</td> <td class='DirEsp'>(=) Valor do documento<br>&nbsp;&nbsp;<b>$Valor</b></td> </tr>";
print "<tr> <td rowspan='5' colspan='5' class='Instru'> <b>INSTRU&Ccedil;&Otilde;ES:<br></b>*** VALORES EM REAIS ***<br><br><pre>$Instrucoes</pre></td> <td class='Dir'>(-) Desconto<br>&nbsp;&nbsp;0.00</td> </tr>";
print "<tr> <td class='Dir'>(-) Outras dedu&ccedil;&otilde;es<br>&nbsp;&nbsp;0.00</td> </tr>";
print "<tr> <td class='Dir'>(+) Mora / Multa / Juros<br>&nbsp;&nbsp;<b>$Juros</b></td> </tr>";
print "<tr> <td class='Dir'>(+) Outros acr&eacute;scimos<br>&nbsp;&nbsp;0.00</td> </tr>";
print "<tr> <td class='Dir'>(=) Valor cobrado<br>&nbsp;&nbsp;<b>$ValorCalculado</b></td> </tr>";
print "<tr> <td colspan='6' class='Rodape'>$NomeSacado<br>$Endereco<br>$Cep&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$Cidade</td> </tr>";
print "</tbody> </table>";
print "<div id='Auten'>Autentica&ccedil;&atilde;o Mec&acirc;nica</div>";
print "<table>	<tbody>";
print "<tr> <td><img src='banco-$NumeroBanco.bmp'></td> <td id='Num'>$NumeroBancoI</td> <td colspan='4' id='Numero'>$LinhaDigitavel</td> </tr>";
print "<tr> <td colspan='5' class='Texto1'>Local de pagamento<br>&nbsp;&nbsp;<b>QUALQUER AG&Ecirc;NCIA BANC&Aacute;RIA AT&Eacute; O VENCIMENTO</b></td> <td class='DirEsp2'>Vencimento<br><b>$Vencimento</b></td> </tr>";
print "<tr> <td colspan='5' class='Texto1'>Cedente<br>&nbsp;&nbsp;<b>$Cedente</b></td> <td class='Dir'>Ag&ecirc;ncia / C&oacute;digo<br>&nbsp;&nbsp;$CONTA_DA_EMPRESA</td> </tr>";
print "<tr> <td>Data<br><b>$Data</b></td> <td>Documento</td> <td>Esp&eacute;cie<br><b>DS</b></td> <td>Aceite<br><b>N</b></td> <td>Dt.Processamento<br><b>$DataProcessamento</b></td> <td class='Dir'>Nosso N&uacute;mero<br>&nbsp;&nbsp;$NossoNumero</td> </tr>";
print "<tr> <td>Conta</td> <td>Carteira<br><b>$Carteira</b></td> <td>Esp&eacute;cie<br><b>Real</b></td> <td>Quantidade<br></td> <td>Valor</td> <td class='DirEsp'>(=) Valor do documento<br>&nbsp;&nbsp;<b>$Valor</b></td> </tr>";
print "<tr> <td rowspan='5' colspan='5' class='Instru'> <b>INSTRU&Ccedil;&Otilde;ES:</b><br>*** VALORES EM REAIS ***<br><br><pre>$Instrucoes</pre></td> <td class='Dir'>(-) Desconto<br>&nbsp;&nbsp;0.00</td> </tr>";
print "<tr> <td class='Dir'>(-) Outras dedu&ccedil;&otilde;es<br>&nbsp;&nbsp;0.00</td> </tr>";
print "<tr> <td class='Dir'>(+) Mora / Multa / Juros<br>&nbsp;&nbsp;<b>$Juros</b></td> </tr>";
print "<tr> <td class='Dir'>(+) Outros acr&eacute;scimos<br></td> </tr>";
print "<tr> <td class='Dir'>(=) Valor cobrado<br>&nbsp;&nbsp;<b>$ValorCalculado</b></td> </tr>";
print "<tr> <td colspan='6' class='Rodape'>$NomeSacado<br>$Endereco<br>$Cep&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$Cidade</td> </tr>";
print "</tbody> </table>";
print "<div style='width: 610px;text-align:left;padding-top:5px;'>";

if ( $Mostra_CodBarras == 'S' )
   print "<img src='codbarra.php?numero=$CodigoBarras'>";

print "</P></TD></TR></TABLE></BODY></HTML>";
print "</div>";
print "</body> </html>";

exit;

function DiasEmAtraso($data) {
   $ano = substr($data,6,4);
   $mes = substr($data,3,2);
   $dia = substr($data,0,2);
   
   $data_atual = date("d/m/Y");
   $ano_atual = substr($data_atual,6,4);
   $mes_atual = substr($data_atual,3,2);
   $dia_atual = substr($data_atual,0,2);

   $data = mktime(0, 0, 0, $mes, $dia, $ano);
   $data_atual = mktime(0, 0, 0, $mes_atual, $dia_atual, $ano_atual);
   
   $dias = ($data_atual - $data)/86400;
   $dias = round($dias);
   
   if ( $dias < 0 )
      $dias = -1;   
   
   return $dias+1;
}


function MensagemErro() {
   print "<html><title>VIGOprovider</title><font face=Verdana size=+1>ERRO !!!<br>";
   print "N&atilde;o existem boletos em aberto ou<br>";
   print "N&atilde;o foi selecionado nenhum boleto da lista anterior<br><br>";
   print "Para maiores informa&ccedil;&otilde;es contate o WEBMASTER.<br>";
   print "<br><a href='javascript:history.go(-1);'>Clique aqui para retornar</a></font></html>";
   exit;
}

// ----------------------------------------------------------------
function CalculaJuros() {
   global $Valor, $Taxa_Juros, $Taxa_Mora, $Vencido;
   
   $multa = $Valor * $Taxa_Juros;
   $mora = ($Valor * $Taxa_Mora) * $Vencido;
   $Juros = $multa + $mora;
   $NovoValor = $Valor + $Juros;
   $Juros = sprintf("%10.2f", $Juros);
   $NovoValor = sprintf("%10.2f", $NovoValor);
   $Valor = $NovoValor;

   NovoCodigoBarras();
   NovaLinhaDigitavel();
}

function NovoCodigoBarras() {
   global $Valor, $NossoNumero, $CodigoBarras, $Vencimento;
   
   $idBanco = '341';
   $moeda = '9';
   $data_atual = date("d/m/Y");
   $Vencimento = $data_atual;
   $fatorVcto = fator_vencimento($data_atual);
   $carteira = '175';
   $valor = str_pad(str_replace('.','',trim($Valor)), 10, '0', STR_PAD_LEFT);
   $nnum = substr($CodigoBarras, 22, 8);
   $agencia = substr($CodigoBarras, 31, 4);
   $conta = substr($CodigoBarras, 35, 5);

   $cb = $idBanco.$moeda.$fatorVcto.$valor.$carteira.$nnum.modulo_10($agencia.$conta.$carteira.$nnum).$agencia.$conta.modulo_10($agencia.$conta).'000';
   $dv = digitoVerificador_barra(cb);
   $CodigoBarras = substr($cb, 0, 4).$dv.substr($cb, 4, 43);
}

function NovaLinhaDigitavel() {
   global $CodigoBarras, $LinhaDigitavel;
   
   $codigo = $CodigoBarras;
   $banco = '341';
   $moeda = '9';
   $ccc = '175';
   $ddnnum = substr($codigo,22,2);
   $dv1 = modulo_10($banco.$moeda.$ccc.$ddnnum);
   $resnnum = substr($codigo,24,6);
   $dac1 = substr($codigo,30,1);
   $dddag = substr($codigo,31,3);
   $dv2 = modulo_10($resnnum.$dac1.$dddag);
   $resag = substr($codigo,34,1);
   $contadac = substr($codigo,35,6);
   $zeros = substr($codigo,41,3);
   $dv3 = modulo_10($resag.$contadac.$zeros);
   $dv4 = substr($codigo,4,1);
   $fator = substr($codigo,5,4);
   $valor = substr($codigo,9,10);

   $campo1 = substr($banco.$moeda.$ccc.$ddnnum.$dv1,0,5) . '.' . substr($banco.$moeda.$ccc.$ddnnum.$dv1,5,5);
   $campo2 = substr($resnnum.$dac1.$dddag.$dv2,0,5) . '.' . substr($resnnum.$dac1.$dddag.$dv2,5,6);
   $campo3 = substr($resag.$contadac.$zeros.$dv3,0,5) . '.' . substr($resag.$contadac.$zeros.$dv3,5,6);
   $campo4 = $dv4;
   $campo5 = $fator.$valor;

   $LinhaDigitavel = "$campo1  $campo2  $campo3  $campo4  $campo5"; 
}

function modulo_10($num) { 
   $numtotal10 = 0;
   $fator = 2;
   for ($i = strlen($num); $i > 0; $i--) {
      $numeros[$i] = substr($num,$i-1,1);
      $temp = $numeros[$i] * $fator; 
      $temp0=0;
      foreach (preg_split('//',$temp,-1,PREG_SPLIT_NO_EMPTY) as $k=>$v){ $temp0+=$v; }
      $parcial10[$i] = $temp0;
      $numtotal10 += $parcial10[$i];
      if ($fator == 2)
         $fator = 1;
      else
         $fator = 2;
   }
   $resto = $numtotal10 % 10;
   $digito = 10 - $resto;
   if ($resto == 0)
      $digito = 0;

   return $digito;
}

function modulo_11($num, $base=9, $r=0)  {
   $soma = 0;
   $fator = 2;
   for ($i = strlen($num); $i > 0; $i--) {
      $numeros[$i] = substr($num,$i-1,1);
      $parcial[$i] = $numeros[$i] * $fator;
      $soma += $parcial[$i];
      if ($fator == $base)
         $fator = 1;
      $fator++;
   }
   if ($r == 0) {
      $soma *= 10;
      $digito = $soma % 11;
      if ($digito == 10)
         $digito = 0;
      return $digito;
   } elseif ($r == 1){
      $resto = $soma % 11;
      return $resto;
   }
}

function fator_vencimento($data) {
	$data = split("/",$data);
	$ano = $data[2];
	$mes = $data[1];
	$dia = $data[0];
    return(abs((_dateToDays("1997","10","07")) - (_dateToDays($ano, $mes, $dia))));
}

function _dateToDays($year,$month,$day) {
    $century = substr($year, 0, 2);
    $year = substr($year, 2, 2);
    if ($month > 2) {
        $month -= 3;
    } else {
        $month += 9;
        if ($year) {
            $year--;
        } else {
            $year = 99;
            $century --;
        }
    }
    return ( floor(( 146097 * $century) / 4 ) +
             floor(( 1461 * $year) / 4 ) +
             floor(( 153 * $month + 2) / 5 ) + $day + 1721119);
}

function digitoVerificador_barra($numero) {
	$resto2 = modulo_11($numero, 9, 1);
	$digito = 11 - $resto2;
     if ($digito == 0 || $digito == 1 || $digito == 10  || $digito == 11) {
        $dv = 1;
     } else {
        $dv = $digito;
     }
	 return $dv;
}

?>