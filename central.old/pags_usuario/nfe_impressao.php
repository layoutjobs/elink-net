<?php

// Lista NFE

$id = mysql_real_escape_string($id);

$qnota_fiscal = mysql_query("SELECT * FROM nf_arquivo_mestre 
                            WHERE codigo='{$_SESSION["usuario"]["numero"]}' AND 
                                  referencia='{$id}' 
                            ORDER BY referencia DESC", $vigo);

if (mysql_num_rows($qnota_fiscal)==0)
{
	exit("N�o encontrado!");
}

$nota_fiscal = mysql_fetch_array($qnota_fiscal);
$nf_arquivo = substr($nota_fiscal['nome_arquivo'], 0, 10);
$nf_sequencial = $nota_fiscal['referencia'];
$nf_valor = intval(substr($nota_fiscal['valor_total'], 0, -2)) . ',' . substr($nota_fiscal['valor_total'], -2);

$r_nf_dados=mysql_query("SELECT * FROM nf_arquivo_dados 
                         WHERE nome_arquivo LIKE '{$nf_arquivo}%' AND
                               sequencial='{$nf_sequencial}'");
$d_nf_dados=mysql_fetch_array($r_nf_dados);

$r_nf_item=mysql_query("SELECT * FROM nf_arquivo_item 
                        WHERE nome_arquivo LIKE '{$nf_arquivo}%' AND
                        numero='{$nf_sequencial}'");

$r_empresas=mysql_query("SELECT * FROM empresas WHERE id='{$_SESSION["usuario"]["idempresa"]}'");
$d_empresas=mysql_fetch_array($r_empresas);

?>
<style type="text/css">

.p_titulo {
	font-size: 24pt;
	text-align: center;
}

.p_dados {
	text-align: center;
}
.p_dados b {
	display: inline-block;
	padding: 0 0 0 10px;
}

.t_dados {
	margin: 10px 0;
}
.t_dados td {
	border: 1px solid #000000;
	width: 25%;
}
.t_dados td b {
	display: block;
	text-align: right;
	font-size: 18pt;
}

</style>

<div style="border: 1px solid #000000;">
	
	<table style="width: 100%;">
	<tr>
		
		<td>
		<img src="imagizer_export.php?<?=Empresas::logomarca($_SESSION["usuario"]["idempresa"])?>,185,95,2,0,,jpg"/>
		</td>
		
		<td style="width: 100%;">
		
			<h1>
			<?=$d_empresas["fantasia"]?>
			</h1>
			
			CNPJ: <?=$d_empresas["cnpj"]?><br />
			IE: <?=$d_empresas["ie"]?><br />
			Endere�o: <?=$d_empresas["endereco"]?><br />
			Bairro/Cidade: <?=$d_empresas["bairro"]?> - <?=$d_empresas["cidade"]?> - <?=$d_empresas["uf"]?><br />
			Contato: <?=$d_empresas["telefone"]?> / <?=$d_empresas["fax"]?> / <?=$d_empresas["email"]?>
		
		</td>
	
	</tr>	
	</table>
	
	<p class="p_titulo">
	NOTA FISCAL DE SERVI�O DE COMUNICA��O<br />
	MODELO 21
	</p>
	
	<p class="p_dados">
		<b>N�MERO:</b> <?=$nota_fiscal["numero"]?>
		<b>S�RIE:</b> <?=$nota_fiscal["serie"]?>
		<b>EMISS�O:</b> <?=date('d/m/Y', strtotime($nota_fiscal['emissao']))?>
		<b>REFER�NCIA:</b> <?=$nota_fiscal["referencia"]?>
	</p>
	
	<br />
	
	<table style="width: 100%;">
	<tr valign="top">
		
		<td>
		<img src="framework/barcode/?id=<?=$d_nf_dados["numero"]?>" style="margin: 0 0 0 20px;"/>
		</td>
		
		<td style="width: 100%;">
		<?=$d_nf_dados["numero"]?><br />
		<?=$d_nf_dados["rsocial"]?><br />
		<?=$d_nf_dados["cnpjcpf"]?><br />
		<?=$d_nf_dados["logradouro"]?><br />
		<?=$d_nf_dados["bairro"]?>, <?=$d_nf_dados["municipio"]?> - <?=$d_nf_dados["uf"]?>
		</td>
		
	</tr>
	</table>
	
	<br />
	
	<table class="t_listagem" style="width: 100%;">
	
		<thead>
		<tr>
			<td>DISCRIMINA��O DO SERVI�O</td>
			<td>VALOR</td>
		</tr>
		</thead>
		
		<tbody>
		<?
		for($i=0;$i<mysql_num_rows($r_nf_item);$i++)
		{
			$d_nf_item=mysql_fetch_array($r_nf_item);
			?>
			<tr>
				
				<td>
				<?=$d_nf_item["desc_servico"]?>
				</td>
				
				<td>
				<?=intval(substr($d_nf_item['total'], 0, -2)) . ',' . substr($d_nf_item['total'], -2)?>

				</td>
				
			</tr>			
			<?
		}
		?>
		</tbody>
		
		<!--
		<tfoot>
		<tr>
			<td colspan="10" style="text-align: right;">
			TTOAL
			</td>	
		</tr>
		</tfoot>
		-->
		
	</table>
	
	<br />
	
	<p>
	Nota fiscal referente ao boleto: <b><?=$nota_fiscal["boleto_nnumero"]?></b>
	</p>
	
	<br />
	
	<table class="t_dados" style="width: 100%;">
	<tr>
		<td>BASE DE C�LCULO <b><?=$nota_fiscal["bc_icms"]?></b> </td>
		<td>AL�QUOTA (%) <b>30,00</b> </td>
		<td>VALOR DO ICMS <b><?=$nota_fiscal["icms"]?></b> </td>
		<td>VALOR TOTAL <b><?=$nf_valor?></b> </td>
	</tr>
	<tr>
		<td colspan="4" style="width: auto;">
		AUTENTICA��O DIGITAL
		<b style="text-align: center;">
		<?php
		echo wordwrap($nota_fiscal['cad_md5_registro'],4,".",true);
		?>
		</b>
		</td>
	</tr>
	</table>
	
	<small style="display: block; text-align: center;">
	Contribui��o para o FUST e FUNTTEL - 1,5% do valor do servi�os - N�o repassada �s tarifas
	</small>

</div>
