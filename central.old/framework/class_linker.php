<?php
// Brett D. Estrade
// estrabd@mvdev.com
// http://www.mvdev.com

/**
*	This class parses a string, and automatically
*	formats URLs and email addresses with the 
*	appropriate HTML so that they are linked.
*
*	It would be easy to extend automatic formatting
*	to other items, but currently my needs are only 
*	for URLs and email address.
*
*	If any improvments are made, please let me know, and
*	I will include them in my distribution.
*
*	This code is free for use, modification, and distribution
*	as long as this header remains up here.
*
*	Regular expressions for pattern matching were borrowed from 
*   http://www.phpwizards.net .
*/

class Linker
{
	/**
	* Constructor -- initializes internal elements
	*/
	public static function format($str = '')
	{
		//String to parse
		//$this->str = '';
		//Unmodified string
		$oldStr = '';
		//URL link format
		$urlFormat = 'href=';
		//URL link target
		$urlTarget = '_blank';
		//Email link format
		$emailFormat = 'mailto:';//'mailto:';
		// Query string example: 
		//$this->emailFormat='./contact.php?sendTo=';
		//Email link target
		$emailTarget = '_self';

		// Regex to parse URLs.
		// Contributed by Bill Peck, http://www.pecknet.com
		// bill@pecknet.com
		// This leaves URLs that already have the HTML formatting to be left alone
		// For example, it will parse 'http://www.mvdev.com',
		// but not '<a href="http://www.mvdev.com">mvdev's site</a>'
		$str = eregi_replace("([^\"[[:alpha:]]|^)([[:alpha:]]+)://([^[:space:]]*)([[:alnum:]#?/&=])", "\\1<a ".$urlFormat."\"\\2://\\3\\4\"target=\"".$urlTarget."\">\\2://\\3\\4</a>", $str);    	
		//These regular expressions were borrowed from http://www.phpwizard.net .  Thanks, guys!
		//old regex for URLS, has been replaced with the on above
		//$this->str = eregi_replace("([[:alnum:]]+)://([^[:space:]]*)([[:alnum:]#?/&=])", "<a ".$this->urlFormat."\"\\1://\\2\\3\" target=\"_blank\" target=\"".$this->urlTarget."\">\\1://\\2\\3</a>", $this->str);
  $str = eregi_replace("(([a-z0-9_]|\\-|\\.)+@([^[:space:]]*)([[:alnum:]-]))", "<a href=\"".$emailFormat."\\1\" target=\"".$emailTarget."\">\\1</a>", $str);
  return $str;
	}

	/**
	* Returns original string that was parsed.
	*/	

	function getOldStr()
	{
		return $this->oldStr;
	}
}

?>