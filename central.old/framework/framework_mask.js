$(function(){
	
	$('.c_data').datepicker({
		inline: true,
		changeYear: true,
		changeMonth: true,
		
		showOn: "button",
		buttonText: "",
		buttonImageOnly: true,
		
		shortYearCutoff: '+50',
		showButtonPanel: true
	});

	$('.c_data_hora').datetimepicker({  
		inline: true,
		changeYear: true,
		changeMonth: true,
		showButtonPanel: true,
		
		showOn: "button",
		buttonText: "",
		buttonImageOnly: true,
		
		shortYearCutoff: '+50',
		duration: '',  
		showTime: true,  
		constrainInput: false,  
		stepMinutes: 1,  
		stepHours: 1,  
		altTimeField: '',  
		time24h: true  
	});
	
	$(".c_data").mask("99/99/9999");
	$(".c_data_hora").mask("99/99/9999 99:99");
	$(".c_hora").mask("99:99");
	
	$(".c_fone").mask("(99) 9999-9999");
	$(".c_cpf").mask("999.999.999-99");
	$(".c_cnpj").mask("99.999.999/9999-99");
	$(".c_cep").mask("99999-999");
	
	$('.c_parcelamento').numeric({allow:"+"});
	$('.c_parcelamento_completo').numeric({allow:"p,"});
	$('.c_num').numeric();
	
	$(".c_moeda").maskMoney({symbol:"R$",decimal:",",thousands:"."});
	
	$("input, textarea").focus(
		function()
		{
			// only select if the text has not changed
			if(this.value == this.defaultValue)
			{
				this.select();
			}
		}
	);
	
});