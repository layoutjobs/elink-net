<link rel="stylesheet" type="text/css" href="css_quadros.css"/>

<div class="index" id="conteudo">

<!--
<div class="quadro_space">
	<span class="spaceh"></span>
</div>
-->
<?php
global $a_conteudo;
$a_conteudo=array();

$b_tabela="sis_franquias_quadros";
$b_campos=Suporte::mysql_field_array($b_tabela);

$t_template=$template;
if($t_template=="")
{
    $t_resultado=mysql_query("SELECT ste_id FROM sis_franquias_templates WHERE ste_franquia='{$_SESSION["cid"]}' AND ste_status='1' LIMIT 1");
    if(mysql_num_rows($t_resultado)>0)
    {
        $t_template=mysql_result($t_resultado,0,"ste_id");
    }
}

$b_sqladd="";
$b_sqladd.=($t_template!="")?" AND sfq_template='{$t_template}'":" AND sfq_template='0'";
$b_sqladd.=" AND (sfq_franquia='{$_SESSION["cid"]}' OR sfq_franquia LIKE '%,{$_SESSION["cid"]},%')";

$b_resultado=mysql_query("SELECT * FROM {$b_tabela} WHERE sfq_status='1'{$b_sqladd} ORDER BY sfq_ordem ASC, sfq_id DESC");
for($b=0;$b<mysql_num_rows($b_resultado);$b++)
{
	for($c=0;$c<count($b_campos);$c++) { ${$b_campos[$c]} = mysql_result($b_resultado,$b,$b_campos[$c]); }
	//$sfq_modelo=Suporte::nomear($sfq_modelo);
	//include("sis_franquias_quadros/modelos/{$sfq_modelo}.php");
	
	$filename = "sis_franquias_quadros/modelos/{$sfq_modelo}.php";
	$handle = fopen ($filename, "r");
	$conteudo = fread ($handle, filesize ($filename));
	fclose ($handle);
	
	for($s=1;$s<10;$s++)
	{
		$t_link="<a onclick=\"document.editar.fqb_secao.value='{$s}'\">{{$s}}</a>";
		$conteudo=str_replace("{{$s}}",blocos($sfq_id,$s),$conteudo);
	}
	
	?>
	<div id="d_quadro_<?=$sfq_id?>">
	<?php
	print $conteudo;
	?>
	</div>
	<div class="quadro_space">
		<span class="spaceh"></span>
	</div>
	<?php
	
}

function blocos($quadro,$secao)
{
	global $a_conteudo;
	global $a_tipos;
	global $v_diretorio_franquia;

	//return("{$quadro},{$secao}");
	ob_start();
	
	//$debug=true;
	
	if($debug!=false)
	{
		?>
		<span style="position: absolute; font-size: 8pt; color: red;">
		Q<?=$quadro?>S<?=$secao?>
		</span>	
		<?php
	}	
	
	$tabela="sis_franquias_quadros_blocos";
	$s_campos=Suporte::mysql_field_array($tabela);
	$s_campos=array_merge($s_campos,Suporte::mysql_field_array("sis_franquias_quadros_modelos"));
	
	$sqladd="";
	$sqladd.=" AND fqb_quadro='{$quadro}'";
	$sqladd.=" AND fqb_secao='{$secao}'";
	
	$s_sqljoin=" LEFT OUTER JOIN sis_franquias_quadros_modelos ON (fqm_id=fqb_modelo)";
	
	$s_resultado=mysql_query("SELECT * FROM {$tabela}{$s_sqljoin} WHERE fqb_status='1'{$sqladd} ORDER BY fqb_ordem ASC");
	for($s=0;$s<mysql_num_rows($s_resultado);$s++)
	{
		for($c=0;$c<count($s_campos);$c++) { ${$s_campos[$c]} = mysql_result($s_resultado,$s,$s_campos[$c]); }
		$fqb_categorias=($fqb_categorias=="")?"0":$fqb_categorias;
		
		if($s>0)
		{
			?>
			<span class="spaceh"></span>
			<?php
		}
		?>
		<div class="secao modelo_<?=$fqm_arquivo?>" id="d_bloco_<?=$fqb_id?>">
		<?php
		if(file_exists("sis_franquias_quadros_blocos/modelos/{$fqm_arquivo}.css"))
		{
			?>
			<link href="<?="sis_franquias_quadros_blocos/modelos/{$fqm_arquivo}.css"?>" rel="stylesheet" type="text/css" />
			<?php
		}
		include("sis_franquias_quadros_blocos/modelos/{$fqm_arquivo}.php");
		?>
		</div>
		<?php
	}

	$bloco = ob_get_contents();
	ob_end_clean();
	
	return($bloco);
	
}

?>

</div>