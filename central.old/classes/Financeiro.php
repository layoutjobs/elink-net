<?
class Financeiro {

    public static function juros($valor, $data_vencimento)
    {
        // Obtem os valores de multa e juros a partir da tabela central
        // para flexibilizar a customizacao destes valores
        global $vigo;
        $query = mysql_query('SELECT multa, juros FROM central', $vigo) or die(mysql_error());
        $row = mysql_fetch_assoc($query);
        $multa = (double) $row['multa'];
        $mora = (double) $row['juros'];

        $juros = $multa/100;
        $mora  = $mora/100;
        $dias_em_atraso = Financeiro::diasEmAtraso($data_vencimento);

        if ( $dias_em_atraso>0 ) { 
            $multa = $valor*$juros;
            $mora  = ($valor*$mora) * $dias_em_atraso;
            $Juros = $multa+$mora;
            $valor += $Juros;
        }
        return sprintf("%10.2f", $valor);
    }
	
	/*
	 * Retorna o numero de dias em atraso
	 */
	
	public static function diasEmAtraso($vencimento, $hoje=NULL) {
	   if(!is_null($hoje))
	      $d = getdate($hoje);
	   else
	      $d = getdate();
	   
	   $hoje = mktime(0, 0, 0, $d['mon'], $d['mday'], $d['year']);
	
	   $vencimento = strtotime($vencimento);
	   if( $vencimento>$hoje )
	      return 0;
	
	   $atraso = round(($hoje - $vencimento)/86400);
	   
	   // Confie nos testes automatizados !!!!
	   if(!Financeiro::diaUtil($vencimento)) {
	      $proximo_dia_util = Financeiro::proximoDiaUtil($vencimento);
	      $diff = ($hoje - $proximo_dia_util)/86400;
	      return $diff;
	   }
	
	   //~ return $atraso+1;
	   return $atraso;
	}
	
	/*
	 * Verifica se a data informada eh um dia util: segunda a sexta
	 */
	
	public static function diaUtil($dt) {
	   $feriados = array(
	      '1/1',   // Carnaval
	      '21/4',  // Tiradentes
	      '1/5',   // Dia do trabalho
	      '7/9',   // Independencia
	      '12/10', // Nossa senhora
	      '2/11',  // Finados
	      '15/11', // Proclamacao da Republica
	      '25/12'  // Natal
	   ); 
	
	   $aux = getdate($dt);
	   if( ($aux['wday']==0) or ($aux['wday']==6))
	      return FALSE;
	   
	   if(in_array($aux['mday'].'/'.$aux['mon'], $feriados))
	      return FALSE;
	   
	   return TRUE;
	}
	
	public static function proximoDiaUtil($param) {
	   while (!Financeiro::diaUtil($param))
	      $param += 86400;
	   return $param;
	}
		
}
?>
