<?

class Paginacao
{
	
	private $itens=30;
	private $inicio=1;
	private $js="";
	private $totalPaginas=0;
	
	private $sql="";
	private $sql_total=0;
	private $sql_inicio=0;
	
	public function Paginacao($itens=30,$inicio=1,$js="paginacao")
	{
		//$itens=30;
		//$item=1;
		$inicio=($inicio=="")?"1":intval($inicio);
		
		$this->js=$js;
		
		$this->sqlinicio=$itens*($inicio-1);
		
		$this->itens=$itens;
		$this->inicio=$inicio;
		
	}
	
	public function limit()
	{
		$limit=" LIMIT {$this->sqlinicio}, {$this->itens}";
		return($limit);
	}
	
	public function informacoes()
	{
		$total=$this->sql_total;
		$total_total=$this->totalGeral();
		
		$this->totalPaginas=$total_total/$this->itens;
		$informacoes=(($inicio-1)*$itens+1)." at&eacute; ".(($inicio-1)*$itens+($total))." de ".$total_total;
		
		if($total_total==0)
		{
			$informacoes="Nenhum registro";
		}
		
		return($informacoes);
				
	}
	
	public function setSql($sql,$sql_total)
	{
		$this->sql=$sql;
		$this->sql_total=$sql_total;
	}
	
	public function totalGeral()
	{
		$sql=$this->sql;
		$total_total=mysql_result( mysql_query("SELECT count(*) FROM ({$sql}) AS foo") ,0,0);
		return($total_total);		
	}
	
	private function exibirJs($inicio)
	{
		return($this->js."({$inicio});");
	}
	
	public function exibir()
	{
		$inicio=$this->inicio;
		?>
		P�gina
		<b id="pagina"><?=$inicio?></b>/<?=ceil($this->totalPaginas)?><br />
		
		<?php
		if($inicio>1)
		{
			?>
			<a class="button" onclick="<?=$this->exibirJs($inicio-1)?>">
			<img class="mm" src="imagens/ls_anterior.gif" alt="Anterior"/> Anterior</a>
			<?php
		}
		?>
		
		<select class="mm" onchange="<?=$this->exibirJs("this.value")?>">
		<?php
		for($i=1;$i<$this->totalPaginas+1;$i++)
		{
			?>
			<option value="<?= $i; ?>"<?=($i==$inicio)?" selected":""?>>
			<?=$i?>
			</option>
			<?php
		}
		?>
		</select>
		
		<?php
		if($inicio<$this->totalPaginas)
		{
			?>
			<a class="button" onclick="<?=$this->exibirJs($inicio+1)?>">
			<img class="mm" src="imagens/ls_proximo.gif" alt="Pr�ximo"/> Pr�ximo</a>
			<?php
		}
		?>		
		
		<?		
	}
	
}

?>