<?
class Central {
	
	public static function config($campo,$retorna=false)
	{
		global $vigo;
		
		$r_central=mysql_query("SELECT * FROM central",$vigo);
		$d_central=mysql_fetch_array($r_central);
		
		if($retorna==true)
		{
			return($d_central[$campo]);
		}
		
		if($d_central[$campo]=="s")
		{
			return(true);
		}
		
		return(false);
	}
	
	public static function menu()
	{
		$a_menu=array();
		
		$a_menu["usuario"]["index"]=array("Central do Usu�rio");
		
		if(Central::permissao("dados")) $a_menu["usuario"]["dados"]=array("Dados da Conta");
		if(Central::permissao("extrato")) $a_menu["usuario"]["extrato"]=array("Financeiro");
		if(Central::permissao("acesso")) $a_menu["usuario"]["acesso"]=array("Acesso a Internet");
		if(Central::permissao("suporte")) $a_menu["usuario"]["suporte"]=array("Suporte/Atendimento");

		$a_menu["operador"]["index"]=array("Central do Operador");
		$a_menu["operador"]["chats"]=array("Atendimento");
		$a_menu["operador"]["chamados"]=array("Chamados");
		$a_menu["operador"]["permissoes"]=array("Permiss�es");
		
		return($a_menu[$_SESSION["cid"]]);
	}
	
	public static function submenu($menu,$busca="")
	{
		$a_submenu=array();
		
		$r_central_permissoes=mysql_query("SELECT * FROM central_permissoes WHERE exibir='1' ORDER BY grupo ASC, nome ASC");
		for($i=0;$i<mysql_num_rows($r_central_permissoes);$i++)
		{
			$d_central_permissoes=mysql_fetch_array($r_central_permissoes);
			
			$t_id=$d_central_permissoes["id"];
			$t_grupo=$d_central_permissoes["grupo"];
			$t_nome=$d_central_permissoes["nome"];
			
			if(Central::permissao($t_id))
			{			
				$a_submenu["usuario"][$t_grupo][$t_id]=array($t_nome);
			}
		}
		/*
		$a_submenu["usuario"]["dados"]["dados"]=array("Meus Dados");
		$a_submenu["usuario"]["dados"]["dados_atualiza"]=array("Atualizar Dados");
		$a_submenu["usuario"]["dados"]["senha"]=array("Troca de Senha");
		$a_submenu["usuario"]["dados"]["foto"]=array("Atualizar Foto");
		$a_submenu["usuario"]["dados"]["email"]=array("Conta de E-mail");

		$a_submenu["usuario"]["extrato"]["extrato"]=array("Extrato Financeiro");
		$a_submenu["usuario"]["extrato"]["2via"]=array("2� Via Boleto");
		$a_submenu["usuario"]["extrato"]["servicos"]=array("Servi�os Contratados");
		$a_submenu["usuario"]["extrato"]["contrato"]=array("Contrato");
		$a_submenu["usuario"]["extrato"]["nfe"]=array("Nota Fiscal Eletr�nica");
		
		//$a_submenu["usuario"]["acesso"]["pppoesenha"]=array("Trocar Senha PPOE");
		//$a_submenu["usuario"]["acesso"]["pppoehoras"]=array("Extrato de Horas PPOE");
		$a_submenu["usuario"]["acesso"]["mksenha"]=array("Troca de Senha Mikrotik");
		//$a_submenu["usuario"]["acesso"]["mkhoras"]=array("Extrato de Horas Mikrotik");
		$a_submenu["usuario"]["acesso"]["acesso"]=array("Extrato de acessos");
		$a_submenu["usuario"]["acesso"]["acesso_grafico"]=array("Gr�fico de acessos");
		
		$a_submenu["usuario"]["suporte"]["recados"]=array("Meus Recados");
		$a_submenu["usuario"]["suporte"]["download"]=array("Downloads");
		$a_submenu["usuario"]["suporte"]["suporte"]=array("OS - Ordem de Servi�o");
		$a_submenu["usuario"]["suporte"]["suporte_cad"]=array("Abrir Ordem de Servi�o");
		$a_submenu["usuario"]["suporte"]["suporte_contato"]=array("Conta via E-mail");
		$a_submenu["usuario"]["suporte"]["chat_iniciar"]=array("Suporte via Chat");
		$a_submenu["usuario"]["suporte"]["chats"]=array("Hist�rico Chat");
		*/
		$t_submenu=$a_submenu[$_SESSION["cid"]][$menu];

		if($busca!="")
		{
			if(isset($t_submenu[$busca]))
			{
				return(true);
			}
			else
			{
				return(false);
			}
		}

		return($t_submenu);
		
	}
	
	public static function permissao($permissao)
	{
		$r_central_permissoes=mysql_query("SELECT permissoes FROM central");
		for($i=0;$i<mysql_num_rows($r_central_permissoes);$i++)
		{
			$d_central_permissoes=mysql_fetch_array($r_central_permissoes);
			
			$a_permissoes=explode(",",$d_central_permissoes["permissoes"]);
			
			if(in_array($permissao,$a_permissoes))
			{
				return(true);
			}
			
		}
		return(false);
	}
	
	public static function permissaoUsuario($permissao)
	{
		global $v_base;
		global $box;
		
		$a_liberado=array("index","login","sair","codbarra","trocasenha");
		
		if(in_array($permissao,$a_liberado))
		{
			return(true);
		}
		
		$a_perm_chat=array("chat_fila","chat_finalizado","chat_iniciar");
		$permissao=(in_array($permissao,$a_perm_chat))?"chat":$permissao;
		
		$a_perm_chats=array("chats_historico");
		$permissao=(in_array($permissao,$a_perm_chats))?"chats":$permissao;
		
		$a_perm_chats=array("boleto");
		$permissao=(in_array($permissao,$a_perm_chats))?"2via":$permissao;
		
		$a_perm_chats=array("ver_os");
		$permissao=(in_array($permissao,$a_perm_chats))?"suporte":$permissao;
		
		$a_perm_chats=array("email_senha");
		$permissao=(in_array($permissao,$a_perm_chats))?"email":$permissao;
		
		$a_perm_chats=array("nfe_impressao");
		$permissao=(in_array($permissao,$a_perm_chats))?"nfe":$permissao;
				
		if(!Central::permissao($permissao))
		{
			exit(header("Location: {$v_base}usuario/index/?box={$box}&infomensagem=Sem permiss�o."));
		}
	}
	
	public static function permissaoChecarBase()
	{
		$sql = "SELECT * FROM central";
		$r_central=mysql_query($sql,$vigo);
		
		if(!isset($r_central["permissoes"]))
		{
			mysql_query("ALTER TABLE central ADD COLUMN permissoes VARCHAR(500);",$vigo);
			
			$permissoes="acesso,acesso_grafico,mksenha,dados_atualiza,dados,email,foto,senha,2via,contrato,contrato_obrigatorio,extrato,nfe,servicos,chat,chats,download,recados,suporte,suporte_cad,suporte_contato";
			
			mysql_query("INSERT INTO central (permissoes) VALUES ('{$permissoes}');",$vigo);
			
		}
	}
	
}
?>
