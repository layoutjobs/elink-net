<?php
require 'mod_2via.php';

// CALCULAR DIFEREN�A DE DUAS DATA
function date_dif($date_ini, $date_end) {
    if (strcmp(substr($date_ini, 2, 1 ), "/") == 0) {
        $date_ini = substr($date_ini, 6, 4).substr($date_ini, 2, 4).substr($date_ini, 0, 2);
        $date_end = substr($date_end, 6, 4).substr($date_end, 2, 4).substr($date_end, 0, 2);
    }

    $initial_date = getdate(strtotime($date_ini));
    $final_date = getdate(strtotime($date_end));

    $dif = ($final_date[0] - $initial_date[0]) / 86400;
    return round($dif);
}

// CONVERTE DATAS
function convdata($date) { // v 1.0
        $bra = substr($date, 8, 2)."/".substr($date, 5, 2)."/".substr($date, 0, 4);
    return $bra;
}

/*
 * Converte o periodo para o formato "xx Dias xx minutos xx segundos"
 */
function calc_date($dia1, $dia2) { // v 1.1
	$x = strtotime($dia1);
	$y = strtotime($dia2);
	$dias = ($y - $x) / 86400;
	$diff = $y - $x;

	if ($diff % 86400 <= 0)
		$days = $diff / 86400;

	if ($diff % 86400 > 0) {
		$rest = ($diff % 86400);
		$days = ($diff - $rest) / 86400;
		if ( $rest % 3600 > 0 ) {
			$rest1 = ($rest % 3600);
			$hours = ($rest - $rest1) / 3600;
			if ( $rest1 % 60 > 0 ) {
				$rest2 = ($rest1 % 60);
				$minutes = ($rest1 - $rest2) / 60;
				$seconds = $rest2;
			}
			else
				$minutes = $rest1 / 60;
		}
		else
			$hours = $rest / 3600;
	}

	$seconds = (isset($seconds)) ? $seconds : '';

	$s1 = ($days > '1') ? 's' : '';
	$s2 = ($hours > '1') ? 's' : '';
	$s3 = ($minutes > '1') ? 's' : '';
	$s4 = ($seconds > '1') ? 's' : '';

	if ($days != "")
		echo $days." dia".$s1." ";
		
	if ($hours != "")
		echo $hours." hora".$s2." ";
		
	if ($minutes != "")
		echo $minutes." minuto".$s3." ";
		
	if ($seconds != "") 
		echo $seconds." segundo".$s4." ";
}

/*
 * FUNCAO DE SEGUNDO
 * Recebe total de segundos
 */
function segundos($total) {
	$h=0; $m=0; $s=0;

	//enquanto numero de segundos tiver no minimo 1 hora.
	while($total >= 3600) {
		$h++;
		$total = $total - 3600;
	}

	//enquanto numero de segundos tiver no minimo 1 minuto.
	while($total >= 60) {
		$m++;
		$total = $total - 60;
	}

	$s = $total;

	if(strlen($h)==1) {
		$h = "0" . $h;
	}

	if(strlen($m)==1) {
		$m = "0" . $m;
	}

	if(strlen($s)==1) {
		$s = "0" . $s;
	}

	return $h . ":" . $m . ":" . $s;
}

function Difer_horas($hora1,$hora2){ 
	$entrada = "$hora1"; 
	$saida ="$hora2"; 
	$hora1 = explode(":",$entrada); 
	$hora2 = explode(":",$saida); 
	$acumulador1 = ($hora1[0] * 3600) + ($hora1[1] * 60) + $hora1[2]; 
	$acumulador2 = ($hora2[0] * 3600) + ($hora2[1] * 60) + $hora2[2]; 
	$resultado = $acumulador2 + $acumulador1; 
	$hora_ponto = floor($resultado / 3600); 
	$resultado = $resultado - ($hora_ponto * 3600); 
	$min_ponto = floor($resultado / 60); 
	$resultado = $resultado - ($min_ponto * 60); 
	$secs_ponto = $resultado; 
	return $hora_ponto.":".$min_ponto.":".$secs_ponto;  
}

//converte data e hora
function convdatahora($date) { // v 1.0
	$bra = substr($date, 8, 2)."/".substr($date, 5, 2)."/".substr($date, 0, 4)." ".substr($date, 11, 8);
	return $bra;
}

/*
 * $juros = 2/100;
 * $mora  = 0.03/100;
 * $data_vencimento - o formato deve ser aaaa-mm-dd
 */
function juros($valor, $data_vencimento, $juros=2, $mora=0.33) {
	$juros = $juros/100;
	$mora  = $mora/100;
	$dias_em_atraso = DiasEmAtraso($data_vencimento);
	
	if ( $dias_em_atraso>0 ) {
		$multa = $valor*$juros;
		$mora  = ($valor*$mora) * $dias_em_atraso;
		$Juros = $multa+$mora;
		$valor += $Juros;
	}
	return sprintf("%10.2f", $valor);
}

?>