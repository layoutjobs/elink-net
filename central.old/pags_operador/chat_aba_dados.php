<?php

// Mostra os dados do Usuario

ob_clean();

header("Content-Type: text/html; charset=iso-8859-1");

$id=Suporte::protege($id);

$dados = mysql_query("SELECT * FROM usuarios WHERE numero='{$id}'", $vigo);
$row_dados = mysql_fetch_assoc($dados);
//~ $totalRows_dados = mysql_num_rows($dados);

if (!$row_dados['cob_endereco']) 
{
	$CobEnd = $row_dados['endereco'];
	$CobBai = $row_dados['bairro'];
	$CobCid = $row_dados['cidade'];
	$CobUF  = $row_dados['uf'];
	$CobCEP = $row_dados['cep'];
	$CobTel = $row_dados['discagem'];
	$CobCel = $row_dados['celular'];
} 
else
{
	$CobEnd = $row_dados['cob_endereco'];
	$CobBai = $row_dados['cob_bairro'];
	$CobCid = $row_dados['cob_cidade'];
	$CobUF  = $row_dados['cob_uf'];
	$CobCEP = $row_dados['cob_cep'];
	$CobTel = $row_dados['cob_telefone'];
	$CobCel = $row_dados['cob_celular'];
}

?>
<h1>Dados Cliente</h1>

<fieldset>
<legend>Dados pessoais&nbsp;</legend>
	Nome: <?= $row_dados['nome']; ?><br />
	CPF/CNPJ: <?= $row_dados['cpfcgc']; ?><br />
	RG: <?= $row_dados['rgie']; ?><br />
	Data de Nascimento: <?= date('d/m/Y', strtotime($row_dados['dt_nascimento'])); ?><br />
	Nome da m&atilde;e: <?= $row_dados['mae']; ?>
</fieldset>

<fieldset>
<legend>Endere&ccedil;o e contato&nbsp;</legend>
	Endere&ccedil;o: <?= $row_dados['endereco']; ?><br />
	Cidade / UF: <?= $row_dados['cidade']; ?> / <?= $row_dados['uf']; ?><br />
	CEP: <?= $row_dados['cep']; ?><br />
	Telefone: <?= $row_dados['discagem']; ?><br />
	Celular: <?= $row_dados['celular']; ?><br />
</fieldset>

<fieldset>
<legend>Endere&ccedil;o para cobran&ccedil;a &nbsp;</legend>
	Endere&ccedil;o: <?= $CobEnd ?><br />
	Bairro: <?= $CobBai ?><br />
	Cidade / UF: <?= $CobCid ?> / <?= $CobUF ?><br />
	CEP: <?= $CobCEP ?><br />
	<p class="texto_normal">Telefone: <?= $CobTel ?><br />
	Celular: <?= $CobCel ?><br />
</fieldset>

<fieldset>
<legend>Dados da conta&nbsp;</legend>
	Meu login: <?=  $row_dados['login']; ?><br />
	Dia de vencimento: <?= $row_dados['vcto']; ?><br />
	Cliente desde: <?= date('d/m/Y', strtotime($row_dados['dt_entrada'])); ?><br />
</fieldset>
<?
exit();
?>