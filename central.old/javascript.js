$(document).ready(function()
{
    lyteboxs();
    
});

function lyteboxs()
{
	/* This is basic - uses default settings */
	$('a[class*=lytebox]').fancybox();
	
	$('a[class*=lytediv]').fancybox({
		frameWidth: 700,
		frameHeight: 400,
		hideOnContentClick: false,
		centerOnScroll: false,
		autoScale: true,
		autoDimensions: false
	});	
	
	$('a[class*=lyteframe]').addClass("iframe");
	$('a[class*=lyteframe]').fancybox({
		frameWidth: 920,
		frameHeight: 450,
		hideOnContentClick: false,
		centerOnScroll: false,
		autoScale: true,
		autoDimensions: false
	});
	
}