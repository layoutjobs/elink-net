/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

/*Table structure for table `chat` */

DROP TABLE IF EXISTS `chat`;

CREATE TABLE `chat` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `data` timestamp NULL DEFAULT NULL,
  `usuario` varchar(20) DEFAULT NULL,
  `empresa` varchar(20) DEFAULT NULL,
  `setor` varchar(100) DEFAULT NULL,
  `nome` varchar(150) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `telefone` varchar(30) DEFAULT NULL,
  `operador` varchar(20) DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  `ping_usuario` timestamp NULL DEFAULT NULL,
  `ping_operador` timestamp NULL DEFAULT NULL,
  `comentario` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

/*Table structure for table `chat_mensagem` */

DROP TABLE IF EXISTS `chat_mensagem`;

CREATE TABLE `chat_mensagem` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `data` timestamp NULL DEFAULT NULL,
  `chat` varchar(20) DEFAULT NULL,
  `operador` varchar(20) DEFAULT NULL,
  `info` int(1) DEFAULT NULL,
  `mensagem` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=247 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
