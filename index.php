<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Elinknet</title>

		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/main.css" rel="stylesheet">
		<link href="http://fonts.googleapis.com/css?family=Oxygen:700,400" rel="stylesheet">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body id="topo" data-spy="scroll" data-target=".main-navbar" data-offset="112">

		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		  ga('create', 'UA-58562892-1', 'auto');
		  ga('send', 'pageview');
		</script>

		<header class="main-header">
			<div class="container">
				<a class="main-header-brand" href="#">
					<img src="img/logo-elink.png" alt="Elinknet">
					<span>Internet <img src="img/selo-4g-sm.png" alt="4G"></span>
				</a>
				<span class="login-toggle"><small>Acesso Restrito</small> <a href="http://elinknet.dyndns-server.com:8090/central/" class="btn btn-primary" data-toggle="modal" data-target=""><i class="glyphicon glyphicon-lock"></i> Cliente</a></span>
			</div>
			<nav class="main-navbar">
				<div class="container">
					<a class="main-navbar-nav-toggle collapsed" href="#" data-toggle="collapse" data-target=".main-navbar-nav" data-parent=".main-navbar">
		        		<i class="glyphicon glyphicon-align-justify"></i>
		      		</a>
					<ul class="nav main-navbar-nav">
						<li><a href="#empresa" data-smooth-scroll>Empresa</a></li>
						<li><a href="#tecnologia-4g" data-smooth-scroll>Tecnologia 4G</a></li>
						<li><a href="#entretenimento" data-smooth-scroll>Entretenimento</a></li>
						<li><a href="#internet-residencial" data-smooth-scroll>Planos</a></li>
						<li><a href="#acesso-remoto" data-smooth-scroll>Acesso Remoto</a></li>
						<li><a href="#internet-corporativa" data-smooth-scroll>Corporativo</a></li>
						<li><a href="#contato" data-smooth-scroll>Contato</a></li>
					</ul>
				</div>			
			</nav>
		</header>

		<section class="main-banner">
			<div id="main-banner-carousel" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner">
					<div class="item active">
						<div class="container">
							<div class="main-banner-content">
								<img class="main-banner-img" src="img/banner-internet-residencial.png">
								<h1 class="main-banner-title text-bold">Internet residencial <span class="text-uppercase">4G</span></h1>
								<p class="main-banner-lead">Tecnologia 4G, mais velocidade pelo menor preço</p>
								<p class="main-banner-leadbox">Planos de 2, 3 ou 4 Megas. <br>Consulte planos maiores!</p>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="container">
							<div class="main-banner-content">
								<img class="main-banner-img" src="img/banner-acesso-remoto.png">
								<h1 class="main-banner-title text-bold">Acesso remoto através <br>de porta <span class="text-uppercase">IP-DNS</span></h1>
								<p class="main-banner-lead">Acesso via web para câmeras de segurança</p>
								<p class="main-banner-leadbox">Segurança para sua casa ou empresa</p>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="container">
							<div class="main-banner-content">
								<img class="main-banner-img" src="img/banner-internet-corporativa.png" style="z-index: 1;">
								<h1 class="main-banner-title text-bold">Internet corporativa <span class="text-uppercase">4G</span></h1>
								<p class="main-banner-lead">Mais velocidade para a conexão da sua empresa</p>
								<p class="main-banner-leadbox">Planos especiais. Consulte!</p>
							</div>
						</div>
					</div>
				</div>
				<a class="left carousel-control" href="#main-banner-carousel" role="button" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left"></span>
				</a>
				<a class="right carousel-control" href="#main-banner-carousel" role="button" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right"></span>
				</a>
			</div>
		</section>

		<section class="main-section" id="empresa">
			<div class="main-subsection main-subsection-a">
				<div class="container">
					<div class="row">
						<div class="col-a col-md-5">
							<h1 class="main-title text-muted"><span class="text-bold">Elinknet</span> solução <br>para internet home</h1>
							<p class="margin-bottom-lg">Somos especializados na transmissão de serviços de dados e internet Banda Larga, através do sistema wireless, com atuação no mercado de telecomunicações a mais oito anos. Nossos transmissores são de última geração e todos homologados pela Anatel. Nossos links chegam aos nossos Backbones com altíssima velocidade através de circuito de Fibra Óptica, e são distribuídos às nossas torres de transmissão e sequencialmente aos nossos clientes com a máxima qualidade. Nossa empresa também possui e mantém atualizadas todas as exigências necessárias feitas pela Anatel, garantindo assim maior segurança e confiabilidade aos nossos clientes.</p>
						</div>
						<div class="col-b col-md-7">
							<img class="img-responsive" src="img/empresa-01.jpg">
						</div>
					</div>
				</div>
			</div>
			<div class="main-subsection main-subsection-b">
				<h2 class="main-title text-center text-primary text-bold">A máxima qualidade em sinal wireless 4G.</h2>
				<ul>
					<li><img src="img/empresa-02.jpg"></li>
					<li><img src="img/empresa-03.jpg"></li>
					<li><img src="img/empresa-04.jpg"></li>
					<li><img src="img/empresa-05.jpg"></li>
				</ul>
			</div>
		</section>

		<section class="main-section" id="tecnologia-4g">
			<div class="container">
				<div class="row">
					<div class="col-a col-md-5">
						<h1 class="main-title text-muted"><span class="text-bold">Elinknet 4G</span> a mais <br>moderna tecnologia <br>de conexão.</h1>
						<p>A Elinknet utiliza da tecnologia 4G fixa (quarta geração do sistema de transmissão de dados via wireless) para a provisão do sinal de internet, seja na sua residência ou empresa, através dos nossos modens 4G, também chamados Elink Station.</p>
						<p>Estes equipamentos permitem o recebimento de sinal de banda larga com desempenho notoriamente superior às antenas via rádio convencionais. Elink Station 4G é um CPE inteligente da <a href="http://www.ubnt.com/airmax/airmax-ac/">Ubiquiti Inc.</a>, que opera na frequência 5.8 GHz, utiliza da tecnologia MIMO (múltiplas antenas no mesmo aparelho) e do protocolo Air Max (que possibilita um autogerenciamento mais eficiente da rede, através do seu processador Atheros), permitindo assim que as transferências de dados sejam feitas em alta velocidade e sem percas de pacotes. O resultado da tecnologia 4G é a maior rapidez e eficácia na sua navegação.</p>
						<p class="margin-bottom-remove"><img src="img/logo-airmax-ac.png" alt="airMAX ac"></p>
					</div>
					<div class="col-b col-md-7">
						<br class="visible-xs visible-sm">
						<p class="margin-bottom-remove"><img class="img-responsive center-block" src="img/tecnologia-4g-01.jpg"></p>
					</div>
				</div>
			</div>
		</section>

		<section class="main-section" id="entretenimento">
			<div class="container">
				<div class="row">
					<div class="col-a col-md-7">
						<img class="img-responsive center-block" src="img/tecnologia-4g-02.jpg">
					</div>
					<div class="col-b col-md-5">
						<h1 class="main-title"><span class="text-bold">estudo <br>música <br>vídeo <br>pesquisa <br>notícias <br>redes sociais <br>trabalho</span></h1>
						<p class="margin-bottom-remove">Com a Elinknet 4G assitir vídeos, escutar músicas, pesquisar, trabalhar ou ainda estudar, fica mais fácil, rápido e divertido. Você poderá executar diversas atividades ao mesmo tempo, sem perca de sinal, garantindo assim um funcionamento realmente eficiente.</p>
					</div>
				</div>
			</div>
		</section>

		<section class="main-section" id="internet-residencial">
			<div class="container">
				<div class="row">
					<div class="col-a col-md-7 col-md-push-5">
						<p class="margin-bottom-remove"><img class="img-responsive center-block" src="img/internet-residencial-01.jpg"></p>
					</div>
					<div class="col-b col-md-5 col-md-pull-7">
						<br class="visible-xs visible-sm">
						<h1 class="main-title"><span class="text-bold text-scale text-muted">Escolha seu plano!</span></h1>
						<p>E comece agora a desfrutar da tecnologia de conexão 4G em sua casa. Mais velocidade e diversão para toda a família.</p>
						<p><strong class="text-uppercase">Escolha abaixo a região:</strong></p>
						<p class="margin-bottom-remove">
							<select class="form-control input-lg" data-toggle="remote" data-target="#internet-residencial .plan-grid">
								<option value="api/plan.php?id=IRSPU">São Paulo - área urbana</option>
								<option value="api/plan.php?id=IRSPR">São Paulo - área rural</option>
								<option value="api/plan.php?id=IRMG">Minas Gerais</option>
							</select>
						</p>
					</div>
				</div>
				<br class="visible-xs visible-sm">
				<div class="plan-grid margin-top margin-bottom-remove"></div>
			</div>
		</section>

		<section class="main-section" id="acesso-remoto">
			<div class="container">
				<div class="row">
					<div class="col-a col-md-5">
						<h1 class="main-title text-muted text-bold"><span class="text-xscale">Acesso remoto</span></h1>
						<p class="margin-bottom-lg">A Elinknet oferece um serviço de IP para acesso remoto. Através deste endereço você poderá acessar câmeras de vídeo em sua residência ou empresa, através de computadores, tablets ou celulares conectados a internet.</p>
					</div>
					<div class="col-b col-md-7">
						<img class="img-responsive center-block" src="img/acesso-remoto-01.jpg">
					</div>
				</div>
				<div class="plan-grid margin-bottom" data-remote="api/plan.php?id=AR"></div>
				<p class="margin-bottom-remove"><img class="logo-elink" src="img/logo-elink.png" alt="Elinknet"></p>
			</div>
		</section>

		<section class="main-section" id="internet-corporativa">
			<div class="container">
				<div class="row">
					<div class="col-a col-md-5">
						<h1 class="main-title text-muted text-bold"><span class="text-xscale">Aumente <br>a conexão da <br>sua empresa</span></h1>
						<p>A Elinknet tem a solução certa e econômica para a sua empresa, os nossos planos corporativos tem padrão de link dedicado, com preços de links convencionais.  Além da estabilidade e  simetria de download e upload você poderá acessar  remotamente seus servidores ou  câmeras de vídeo, através de computadores, tablets ou celulares conectados a internet.</p>
						<p class="text-uppercase text-bold"><a data-toggle="modal" data-target="#modal" data-remote="api/plan-application.php?id=IC-info" class="btn btn-primary">Mais Informações</a></p>
						<p class="margin-top-lg"><img class="logo-elink" src="img/logo-elink.png" alt="Elinknet"></p>
					</div>
					<div class="col-b col-md-7">
						<p class="margin-bottom-remove"><img class="img-responsive center-block" src="img/internet-corporativa-01.jpg"></p>
					</div>
				</div>
			</div>
		</section>

		<section class="main-section" id="contato">
			<div class="main-subsection main-subsection-a">
				<div class="container">
					<h1 class="main-title text-muted"><span class="text-bold">Fale conosco</span>, teremos muito prazer em atendê-lo.</h1>
					<div class="row">
						<div class="col-a col-md-6">
							<div data-remote="api/contact.php"></div>
							<p class="margin-top-lg margin-bottom-remove visible-lg visible-md"><img class="logo-elink" src="img/logo-elink.png" alt="Elinknet"></p>
						</div>
						<div class="col-b col-md-6">
							<br class="visible-xs visible-sm">
							<address>
								<a class="street-view-link" href="https://www.google.com.br/maps/uv?hl=pt-BR&pb=!1s0x94c8b35826632fab:0x113e6e93ce01b628!2m5!2m2!1i80!2i80!3m1!2i100!3m1!7e115!4s/maps/place/elinknet/@-23.0858778,-47.2158025,3a,75y,294.83h,90t/data%3D*213m4*211e1*213m2*211sxqooQ1P8EqlrpxmhHdbO6A*212e0*214m2*213m1*211s0x0:0x113e6e93ce01b628!5selinknet+-+Pesquisa+Google&imagekey=!1e2!2sxqooQ1P8EqlrpxmhHdbO6A&sa=X&ved=0ahUKEwit9aS5s5vNAhWGkpAKHQ19BD8Qpx8IaDAK" target="_blank">
									<span class="street-view-link-img"></span>
									<span class="street-view-link-text">Clique para ver no<br>Google Street View</span>
								</a>
								<h5 class="margin-top-remove text-uppercase text-bold">Unidade de São Paulo:</h5>
								Rua 11 de junho, 780 - 4º Andar - Sala 43 - Edifício Córdoba <br>Centro  - Indaiatuba/SP - CEP 13.330-050<br>
								<p><a href="atendimento@elinknet.com.br">atendimento@elinknet.com.br</a></p>
								<p class="h4"><strong>Fone: 11 4024.0850</strong></p>
								<p><strong><img src="img/icon-whatsapp.png" width="24" height="25" style="margin-top: -2px;"> 34 9910.1509</strong></p>
								<p><strong>Oi: 34 8894.1409</strong><br>
								<strong>Vivo: 34 9910.1509</strong></p>
							</address>
							<address class="margin-bottom-remove">
								<h5 class="margin-top-remove text-uppercase text-bold">Unidade de Minas Gerais:</h5>
								<p>Av. Rui Barbosa, 326 - Sala 02 <br>Centro - São Gotardo/MG - CEP 38.800-000</p>
								<a href="atendimento@elinknet.com.br">atendimento@elinknet.com.br</a><br>
								<p class="h4"><strong>Fone: 34 3671.1509</strong></p>
							</address>
						</div>
					</div>
				</div>
			</div>
			<div class="main-subsection main-subsection-b">
				<div id="map" class="map"></div>
			</div>
		</section>

		<footer class="main-footer">
			<div class="container">
				<p class="text-muted"><small>&copy; 2014 · Elinknet 4G · Todos os direitos reservados.</small></p>
				<div class="row">
					<div class="col-a col-md-6">
						<p><img class="logo-elink" src="img/logo-elink.png" alt="Elinknet"></p>
					</div>
					<div class="col-b col-md-6">
						<p>
							<a class="logo-anatel" href="http://www.anatel.gov.br/Portal/verificaDocumentos/documento.asp?numeroPublicacao=330868&assuntoPublicacao=null&caminhoRel=null&filtro=1&documentoPath=330868.pdf" target="_blank">
								<img src="img/logo-anatel.png" alt="Elinknet empresa autorizada Anatel">
								<span>PROC. 53500.029336/2014-93</span>
							</a>
						</p>								
					</div>					
				</div>
				<a class="totop" href="#topo" data-smooth-scroll><span class="glyphicon glyphicon-chevron-up"></span></a>
			</div>
		</footer>

		<!--<div class="modal fade" id="login">
			<div class="modal-dialog">
				<form class="modal-content form-horizontal" action=" http://elinknet.dyndns-server.com:8090/central/" method="post">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<h5 class="modal-title text-bold">Cliente Elinknet: <span class="text-uppercase">Acesse sua área do cliente</span></h5>
					</div>
					<div class="modal-body">
						<input type="hidden" name="acao" value="login">
						<div class="margin-top">
							<div class="form-group">
								<label class="col-md-3 control-label" for="loginUsername">CPF ou CNPJ</label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="loginUsername" name="login" placeholder="Digite seu CPF ou CNPJ" required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="loginPassword">Senha</label>
								<div class="col-md-9">
									<input type="password" class="form-control" id="loginPassword" name="senha" placeholder="Senha" required>
								</div>
							</div>
							<p class="text-center">Seja bem vindo ao portal do cliente.</p>
						</div>
					</div>
					<div class="modal-footer">
						<img class="logo-elink" src="img/logo-elink.png" alt="Elinknet">
						<button type="submit" class="btn btn-primary">Entrar</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
					</div>
				</form>
			</div>
		</div>-->

		<div class="modal fade" id="modal" tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content"></div>
			</div>
		</div>

		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.js"></script>
		<script src="js/main.js"></script>
		<script src="//maps.googleapis.com/maps/api/js"></script>
	</body>
</html>