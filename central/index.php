<?php $baseUrlAlt = 'http://' . trim($_SERVER['HTTP_HOST'], '/') . '/' . trim($_SERVER['REQUEST_URI'], '/') . '/'; ?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=windows-1252"/>
	<title>Central Usuario</title>
	
	<base href="http://elinknet.dyndns-server.com:8090/central/"/>
			
	<link rel="stylesheet" type="text/css" href="framework/framework.css"/>
	<link rel="stylesheet" type="text/css" href="css.css?1371911357"/>
	<link rel="stylesheet" type="text/css" href="cores/azul.css"/>
		
	<link rel="shortcut icon" href="favicon.ico"/>
	

	<!--[if lt IE 9]>
	<script src"http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- �nicio: Ajustes layout -->

	<link href="//fonts.googleapis.com/css?family=Roboto:400,700,700italic,400italic" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo $baseUrlAlt; ?>css.css">

	<!-- Fim: Ajustes layout -->

</head>

<body>

<div id="rd-geral">

	<!-- �nicio: Topo layout -->

	<div class="main-header">
		<a class="greeting" href="usuario/dados">
			Ol� <strong>Braz Sondas Po�os Artesianos Ltda</strong>! <small>Seja bem vindo ao portal do cliente.</small>
		</a>
		<span class="contact">
			Atendimento: <span><strong>SP</strong> (11) 4024.0850</span> <span><strong>MG</strong> (34) 3671.1509</span>
		</span>
		<a class="brand" href="http://www.elinknet.com.br" target="_blank">
			<img src="<?php echo $baseUrlAlt; ?>imagens/logo.png">
		</a>
		<a class="logout" href="usuario/sair">Sair</a>
	</div>

	<!-- Fim: Ajustes layout -->

	<!--	
	<div id="rd-topo">
		
		<div id="rd-topo-meio">
			
			<a class="a_logo" href="usuario/index" title="Central Usuario">
			<img src="imagizer_export.php?tmp/empresa_1.jpg,185,95,2,0,,jpg" style="width: 185px; height: 95px;"/>
			</a>
			
			<div class="d_logado">
							<a href="usuario/dados">
				Ol&aacute;&nbsp; Braz Sondas Po�os Artesianos Ltda </a><br />
				
				<a href="usuario/sair">
				Sair</a>
							</div>            
            
			<div class="d_empresa">
						    <strong>

									<a href="http://www.elinknet.com.br" target="_blank">
			    	ELINKNET - SP</a><br />
			    				    </strong>
			    <small>
			    R. Onze de Junho, 780 - 4� Andar, sala 43 - Edif�cio C�rdoba / Centro<br />
                Indaiatuba - SP / 13330-050<br />
			    (11) 4024-0850 / contato@elinknet.com.br			    </small>
							</div>
            
            <div class="d_menu">
            				<a class="on" href="usuario/index">
				Central do Usu�rio</a>
								<a class="" href="usuario/dados">
				Dados da Conta</a>
								<a class="" href="usuario/extrato">
				Financeiro</a>
								<a class="" href="usuario/acesso">
				Acesso a Internet</a>
				            </div>
            
            <div style="clear: both;"></div>
			
		</div>
	
	</div>
	-->
	
	<div id="rd-conteudo">
		<table id="rd-conteudo-meio" style="width: 100%;" cellpadding="0" cellspacing="0">
		<tr valign="top">		
			<td style="width: 100%; border: 0px;">
				<div id="rd-pagina" class="">
					<div class="d_botoes">
		    		<a class="button" href="usuario/dados">
							<img src="<?php echo $baseUrlAlt; ?>imagens/128x128/info.png"/>
							<span>Meus Dados</span>
						</a>
						<a class="button" href="usuario/extrato">
							<img src="<?php echo $baseUrlAlt; ?>imagens/128x128/calculator.png"/>
							<span>Extrato Financeiro</span>
						</a>
						<a class="button" href="usuario/acesso">
							<img src="<?php echo $baseUrlAlt; ?>imagens/128x128/file.png"/>
							<span>Relat�rio de Acessos</span>
						</a>
					</div>

		<div style="padding: 0 10px;">
			<h1>
			<a href="usuario/extrato">
			Extrato Boletos
			</a>
		</h1>

	<table class="t_listagem" style="width: 100%;" cellpadding="5" cellspacing="0">
	
	<thead>
	<tr>
		<td></td>
		<td>Data</td>
		<td>N&uacute;mero do T&iacute;tulo</td>
		<td>Valor do boleto</td>
		<td>Vencimento</td>
		<td>Detalhes</td>
	</tr>
	</thead>
	
	<tbody>
	   
			<tr>
			
			<td style="width: 1%;">
			<img src="imagens/icones/flag.gif"/>
			</td>
			
			<td>
			10/11/2014			</td>
			
			<td>
			8200011685-2			</td>
			
			<td>
			<!--
			R$      81.35			-->
			R$      81.35				
			</td>
			
			<td>
			5 dias a frente			</td>
			
			<td>
									<a class="button" href="usuario/boleto/?box=sim&NnR=8200011685-2&nId=474" target="_blank">
					<img class="mm" src="imagens/icones/fileprint.gif"/>
					Imprimir Boleto
					</a>					
									
					
				<!-- ESSA DIV CONTEM OS DADOS QUE IRAO APARECER AO SE PRESSIONAR O LINK i -->   
				
				<div id="info_8200011685-2" style="display:none">
				<div class="f_cadastro f_cadastro_linha">
					
					<h3>
					Cliente:
					Braz Sondas Po�os Artesianos Ltda					</h3>
					
					<span>
					<label>N�mero do Titulo:</label>
					8200011685-2					</span>
					
					<span>
					<label>Valor:</label>
					R$      81.35					</span>
					
					<span>
					<label>Vencimento:</label>
					10/11/2014					</span>
					
					<span>
					<label>Dias em atraso:</label>
					5 dias a frente					</span>
					
					<span>
					<label>Data do pagamento:</label>
					N�o pago					</span>
					
					<span>
					<label>Valor total:</label>
					R$ 81.35					
					</span>
				
				</div>
				</div>
				
				<!-- FIM DAS MENSAGENS -->			
				
			</td>
			 
		</tr>
				<tr>
			
			<td style="width: 1%;">
			<img src="imagens/icones/apply.gif"/>
			</td>
			
			<td>
			14/11/2012			</td>
			
			<td>
			8200000570-8			</td>
			
			<td>
			<!--
			R$      80.00			-->
			R$      80.00				
			</td>
			
			<td>
			Pago			</td>
			
			<td>
								
					
				<!-- ESSA DIV CONTEM OS DADOS QUE IRAO APARECER AO SE PRESSIONAR O LINK i -->   
				
				<div id="info_8200000570-8" style="display:none">
				<div class="f_cadastro f_cadastro_linha">
					
					<h3>
					Cliente:
					Braz Sondas Po�os Artesianos Ltda					</h3>
					
					<span>
					<label>N�mero do Titulo:</label>
					8200000570-8					</span>
					
					<span>
					<label>Valor:</label>
					R$      80.00					</span>
					
					<span>
					<label>Vencimento:</label>
					14/11/2012					</span>
					
					<span>
					<label>Dias em atraso:</label>
					Pago					</span>
					
					<span>
					<label>Data do pagamento:</label>
					14/11/2012					</span>
					
					<span>
					<label>Valor total:</label>
					R$      80.00					
					</span>
				
				</div>
				</div>
				
				<!-- FIM DAS MENSAGENS -->			
				
			</td>
			 
		</tr>
				<tr>
			
			<td style="width: 1%;">
			<img src="imagens/icones/apply.gif"/>
			</td>
			
			<td>
			10/12/2012			</td>
			
			<td>
			8200000656-9			</td>
			
			<td>
			<!--
			R$     108.17			-->
			R$     108.17				
			</td>
			
			<td>
			Pago			</td>
			
			<td>
								
					
				<!-- ESSA DIV CONTEM OS DADOS QUE IRAO APARECER AO SE PRESSIONAR O LINK i -->   
				
				<div id="info_8200000656-9" style="display:none">
				<div class="f_cadastro f_cadastro_linha">
					
					<h3>
					Cliente:
					Braz Sondas Po�os Artesianos Ltda					</h3>
					
					<span>
					<label>N�mero do Titulo:</label>
					8200000656-9					</span>
					
					<span>
					<label>Valor:</label>
					R$     108.17					</span>
					
					<span>
					<label>Vencimento:</label>
					10/12/2012					</span>
					
					<span>
					<label>Dias em atraso:</label>
					Pago					</span>
					
					<span>
					<label>Data do pagamento:</label>
					10/12/2012					</span>
					
					<span>
					<label>Valor total:</label>
					R$     108.17					
					</span>
				
				</div>
				</div>
				
				<!-- FIM DAS MENSAGENS -->			
				
			</td>
			 
		</tr>
				<tr>
			
			<td style="width: 1%;">
			<img src="imagens/icones/apply.gif"/>
			</td>
			
			<td>
			17/01/2013			</td>
			
			<td>
			8200001619-0			</td>
			
			<td>
			<!--
			R$     131.45			-->
			R$     131.45				
			</td>
			
			<td>
			Pago			</td>
			
			<td>
								
					
				<!-- ESSA DIV CONTEM OS DADOS QUE IRAO APARECER AO SE PRESSIONAR O LINK i -->   
				
				<div id="info_8200001619-0" style="display:none">
				<div class="f_cadastro f_cadastro_linha">
					
					<h3>
					Cliente:
					Braz Sondas Po�os Artesianos Ltda					</h3>
					
					<span>
					<label>N�mero do Titulo:</label>
					8200001619-0					</span>
					
					<span>
					<label>Valor:</label>
					R$     131.45					</span>
					
					<span>
					<label>Vencimento:</label>
					17/01/2013					</span>
					
					<span>
					<label>Dias em atraso:</label>
					Pago					</span>
					
					<span>
					<label>Data do pagamento:</label>
					17/01/2013					</span>
					
					<span>
					<label>Valor total:</label>
					R$     131.45					
					</span>
				
				</div>
				</div>
				
				<!-- FIM DAS MENSAGENS -->			
				
			</td>
			 
		</tr>
				<tr>
			
			<td style="width: 1%;">
			<img src="imagens/icones/apply.gif"/>
			</td>
			
			<td>
			10/02/2013			</td>
			
			<td>
			8200001834-6			</td>
			
			<td>
			<!--
			R$      51.45			-->
			R$      51.45				
			</td>
			
			<td>
			Pago			</td>
			
			<td>
								
					
				<!-- ESSA DIV CONTEM OS DADOS QUE IRAO APARECER AO SE PRESSIONAR O LINK i -->   
				
				<div id="info_8200001834-6" style="display:none">
				<div class="f_cadastro f_cadastro_linha">
					
					<h3>
					Cliente:
					Braz Sondas Po�os Artesianos Ltda					</h3>
					
					<span>
					<label>N�mero do Titulo:</label>
					8200001834-6					</span>
					
					<span>
					<label>Valor:</label>
					R$      51.45					</span>
					
					<span>
					<label>Vencimento:</label>
					10/02/2013					</span>
					
					<span>
					<label>Dias em atraso:</label>
					Pago					</span>
					
					<span>
					<label>Data do pagamento:</label>
					13/02/2013					</span>
					
					<span>
					<label>Valor total:</label>
					R$      51.45					
					</span>
				
				</div>
				</div>
				
				<!-- FIM DAS MENSAGENS -->			
				
			</td>
			 
		</tr>
			</tbody>
	
	</table>

			<br />
				
					<h1>
						<a href="usuario/acesso_grafico">Acessos</a>
					</h1>
					<div id="d_grafico_0" style="padding: 20px 0; text-align: center; width: 100%; height: 100%;">
					...	
					</div>				
				</div>
			</td>
		</tr>
		</table>
	</div> <!-- rd-conteudo -->	
	<div id="rd-rodape">
		<div id="rd-rodape-meio">
			<div class="d_rodape_copyright">
				<a class="a_desenvolvido">
				<img class="img_middle" src="design/desenv_vigo.png"/></a>
			</div>
		</div>
	</div> <!-- rd-rodape -->
</div> <!-- rd-geral -->	
</body>
</html>

