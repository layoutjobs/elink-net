!function($) {

	'use strict';

	$(document).ready(function() {
		$('[data-toggle=remote]').change();
		$('[data-remote]:not([data-toggle="modal"])').each(function(element) {
			var url = $(this).attr('data-remote');
			var $target = $(this);
			loadRemote(url, $target);
		});

		loadMap();
	});

	$('[data-toggle=collapse]').on('click', function(e) {
		e.preventDefault();
	});

	$('[data-toggle=remote]').on('change', function() {
		var url = $(this).val();
		var $target = $($(this).attr('data-target'));
		loadRemote(url, $target);
	});

	$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
	});

	$(window).scroll(function() { scroll(); });

	/*
	$('[data-smooth-scroll]').on('click', function(e) { 
		e.preventDefault();
		var target = $(this).attr('href');
		var $target = $(target);
		var offset = $target.offset().top - ($('.main-header').is('.fixed') ? 112 : 254);
		offset += offset % 2 ? 1 : 0;
		$('html, body').stop(true, true).animate({
			'scrollTop': offset
		}, 500, 'swing', function() {
			window.location.hash = target;
		});
	});
	*/

	$('[data-smooth-scroll]').click(function() {
		console.log($(window).width());
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			var offset = $(window).width() <= 974 ? 0 : ($('.main-header').is('.fixed') ? 112 : 254);
			offset--;
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top - offset
				}, 500);
				return false;
			}
		}
	});

	function scroll() {
		var offsetTop = $(window).scrollTop();

		if (offsetTop > 142) {
			$('.main-header').addClass('fixed');
		}	else {	
			$('.main-header').removeClass('fixed');
		}
	}

	$('body').on('submit', '[data-remote] form, #modal form', function(e) {
		e.preventDefault();
		var $form = $(this);
		remoteSubmit($form);
	});

	function remoteSubmit($form) {
		var $request = $.ajax({
			type: 'POST',
			url: $form.attr('action'),
			dataType: 'html',
			data: $form.serialize(),
			beforeSend: function() { $form.addClass('loading') },
			complete: function() { $form.removeClass('loading') }
		});

		$request.always(function(data) {
			$form.html(data);
			$form.removeClass('loading');
		});
	}

	function loadRemote(url, $target) {
		var $request;

		$target.addClass('loading');
		$request = $.get(url);
		 
		$request.always(function(html) {
			setTimeout(function(){ 
				$target
					.html(html)
					.removeClass('loading');
			}, 300);
		});  
	}

	function loadMap() {
		var styles = [
			{
				stylers: [
					{ saturation: -100 }
				]
			},{
				featureType: "road",
				elementType: "geometry",
				stylers: [
					{ lightness: 100 },
					{ visibility: "simplified" }
				]
			},{
				featureType: "road",
				elementType: "labels",
				stylers: [
					{ visibility: "off" }
				]
			}
		];
		var styledMap = new google.maps.StyledMapType(styles, {name: "Styled Map"});
		var mapOptions = {
			zoom: 6,
			disableDefaultUI: true,
			center: new google.maps.LatLng(-20.6888522,-47.4427075),
			mapTypeControlOptions: {
				mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
			}
		};
		var map = new google.maps.Map(document.getElementById('map'), mapOptions);
		var marker1 = new google.maps.Marker({
			position: new google.maps.LatLng(-23.0914417,-47.2038804),
			map: map,
			icon: 'img/map-marker.png',
			title: 'Unidade SP',
			animation: google.maps.Animation.DROP
		});
		var marker2 = new google.maps.Marker({
			position: new google.maps.LatLng(-19.3124931,-46.0492458),
			map: map,
			icon: 'img/map-marker.png',
			title: 'Unidade MG',
			animation: google.maps.Animation.DROP
		});

		map.mapTypes.set('map_style', styledMap);
		map.setMapTypeId('map_style');

		google.maps.event.addListener(marker1, 'click', function() {
			map.setZoom(15);
			map.setCenter(marker1.getPosition());
		});

		google.maps.event.addListener(marker2, 'click', function() {
			map.setZoom(15);
			map.setCenter(marker2.getPosition());
		});
	}

}(window.jQuery);   