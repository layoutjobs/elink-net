<?php 

return array(
	'mail' => array(
		'smtpHost' => 'envio.redehost.com.br',
		'smtpPort' => '25',
		'smtpUsername' => 'atendimento@elinknet.com.br',
		'smtpPassword' => 'Cs339805@2015',
		'smtpEncryption' => null, 
		'senderEmail' => 'atendimento@elinknet.com.br',
	),	
	'plans' => array(
		'IRSPU' => array(
			'name' => 'Internet Residencial - SP - Área Urbana',
			'notes' => '<b>Taxa de instalação:</b> R$ 200,00 - Forma de pagamento: a vista.<br><b>Roteador Wi-Fi GRÁTIS</b> (fornecido ao cliente em regime de comodato).',
			'applicationFields' => array('name', 'doc', 'doc2', 'birthDate', 'phone', 'email', 'street', 'number', 'complement', 'zip', 'city'),
			'applicationNotes' => '',
			'colored' => true,
			'plans' => array(
				'2' => array(
					'name' => 'Elink 2 Mega', 
					'description' => 'Velocidades de 2 mega para download e 2 mega para upload – Sem limites de franquia, (tráfego ilimitado).',
					'price' => 'R$ 79,90',
					'color' => '#47a5ad',
				),
				'3' => array(
					'name' => 'Elink 3 Mega', 
					'description' => 'Velocidades de 3 mega para download e 3 mega para upload – Sem limites de franquia, (tráfego ilimitado).',
					'price' => 'R$ 99,90',
					'color' => '#485d88',
				),
				'4' => array(
					'name' => 'Elink 4 Mega', 
					'description' => 'Velocidades de 4 mega para download e 4 mega para upload – Sem limites de franquia, (tráfego ilimitado).',
					'price' => 'R$ 134,90',
					'color' => '#f6821f',
				),
				'5' => array(
					'name' => 'Elink 5 Mega', 
					'description' => 'Velocidades de 5 mega para download e 5 mega para upload – Sem limites de franquia, (tráfego ilimitado).',
					'price' => 'R$ 159,90',
					'color' => '#265cff',
				),
			),
			'installations' => array(),
			'routers' => array(),
		),
		'IRSPR' => array(
			'name' => 'Internet Residencial - SP - Área Rural',
			'notes' => '<b>Taxa de instalação:</b> R$ 200,00 - Forma de pagamento: a vista.<br><b>Roteador Wi-Fi GRÁTIS</b> (fornecido ao cliente em regime de comodato).',
			'applicationFields' => array('name', 'doc', 'doc2', 'birthDate', 'phone', 'email', 'street', 'number', 'complement', 'zip', 'city'),
			'applicationNotes' => '',
			'colored' => true,
			'plans' => array(
				'2' => array(
					'name' => 'Elink 2 Mega', 
					'description' => 'Velocidades de 2 mega para download e 2 mega para upload – Sem limites de franquia, (tráfego ilimitado).',
					'price' => 'R$ 79,90',
					'color' => '#47a5ad',
				),
				'3' => array(
					'name' => 'Elink 3 Mega', 
					'description' => 'Velocidades de 3 mega para download e 3 mega para upload – Sem limites de franquia, (tráfego ilimitado).',
					'price' => 'R$ 99,90',
					'color' => '#485d88',
				),
				'4' => array(
					'name' => 'Elink 4 Mega', 
					'description' => 'Velocidades de 4 mega para download e 4 mega para upload – Sem limites de franquia, (tráfego ilimitado).',
					'price' => 'R$ 134,90',
					'color' => '#f6821f',
				),
				'5' => array(
					'name' => 'Elink 5 Mega', 
					'description' => 'Velocidades de 5 mega para download e 5 mega para upload – Sem limites de franquia, (tráfego ilimitado).',
					'price' => 'R$ 159,90',
					'color' => '#265cff',
				),
			),
			'installations' => array(),
			'routers' => array(),
		),
		'IRMG' => array(
			'name' => 'Internet Residencial - MG - Área Rural',
			'notes' => '<b>Taxa de instalação:</b> R$ 200,00 - Forma de pagamento: a vista.<br><b>Roteador Wi-Fi GRÁTIS</b> (fornecido ao cliente em regime de comodato).',
			'applicationFields' => array('name', 'doc', 'doc2', 'birthDate', 'phone', 'email', 'street', 'number', 'complement', 'zip', 'city'),
			'applicationNotes' => '',
			'colored' => true,
			'plans' => array(
				'2' => array(
					'name' => 'Elink 2 Mega', 
					'description' => 'Velocidades de 2 mega para download e 2 mega para upload – Sem limites de franquia, (tráfego ilimitado).',
					'price' => 'R$ 79,90',
					'color' => '#00a54f',
				),
				'3' => array(
					'name' => 'Elink 3 Mega', 
					'description' => 'Velocidades de 3 mega para download e 3 mega para upload – Sem limites de franquia, (tráfego ilimitado). ',
					'price' => 'R$ 99,90',
					'color' => '#2e3192',
				),
				'4' => array(
					'name' => 'Elink 4 Mega', 
					'description' => 'Velocidades de 4 mega para download e 4 mega para upload – Sem limites de franquia, (tráfego ilimitado).',
					'price' => 'R$ 134,90',
					'color' => '#a44585',
				),
			),
			'installations' => array(),
			'routers' => array(),
		),
		'AR' => array(
			'name' => 'Acesso Remoto',
			'notes' => '<b>Taxa de instalação e ativação:</b> R$ 120,00 (Inclui um SWITCH). Serviço disponível apenas para clientes ativos do estado de São Paulo.',
			'applicationFields' => array('nameAlt', 'docAlt', 'phone', 'email',),
			'applicationNotes' => '',
			'colored' => false,
			'plans' => array(
				'1' => array(
					'name' => 'Porta DNS IP para acesso remoto', 
					'description' => 'O cliente receberá dois endereços com duas portas IP para acesso remoto, que poderão ser usadas para acessar remotamente Câmeras DVR ou servidores.',
					'price' => 'R$ 29,90',
				),
			),
			'installations' => array(
				'1' => 'R$ 200,00 a vista',
				'2' => '2 x de R$ 110,00',
			),
			'routers' => array(
				'S' => 'Sim, R$ 110,00 a vista',
				'N' => 'Não',
			),
		),
		'IC' => array(
			'name' => 'Elink Corporativo',
			'notes' => '',
			'applicationFields' => array('nameAlt', 'docAlt', 'phone', 'email',),
			'applicationNotes' => '',
			'colored' => false,
			'plans' => array(),
			'installations' => array(),
			'routers' => array(),
		),
	),
);