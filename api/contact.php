<?php

$config = include('config.php');

$post    = isset($_POST['Contact']) ? $_POST['Contact'] : false;
$config  = include('config.php');
$values  = array();
$errors  = array();

$fields  = array(
	'name' => array(
		'label' => 'Nome',
		'required' => true,
	),
	'email' => array(
		'label' => 'E-mail',
		'required' => true,
	),
	'message' => array(
		'label' => 'Mensagem',
		'required' => true,
	),
);

foreach ($fields as $name => $field) {
	$values[$name] = isset($post[$name]) ? $post[$name] : '';
}

if ($post) {
	foreach ($fields as $name => $field) {
		if ($field['required'] && $values[$name] == '')
			$errors[$name] = $field['label'] . ' é obrigatório';
	}
}

// --------------------------------------

if ($post && !$errors) {

	$name = $post['name'];
	$email = $post['email'];

	$body  = "<div style=\"font: 11pt/1.4 Arial, 'Helvetica', sans-serif\";>";
	$body .= "<p style=\"margin-bottom: 11pt;\">Olá, $name enviou uma mensagem através do site.</p>";
	$body .= "<table style=\"margin-bottom: 11pt;\"><tbody>";
	foreach ($values as $name => $value) {
		$label = $fields[$name]['label'];
		$body .= "<tr><th align=\"left\">$label: </th><td>$value</td>";
	}
	$body .= "</tbody></table></div>";

	// Import Swift library

	require_once 'vendor' . DIRECTORY_SEPARATOR . 'swiftmailer' . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'swift_required.php';

	// Create the Transport

	if ($config['mail']['smtpHost']) {
		$transport = Swift_SmtpTransport::newInstance()
			->setHost($config['mail']['smtpHost'])
			->setPort($config['mail']['smtpPort'])
			->setEncryption($config['mail']['smtpEncryption'])
			->setUsername($config['mail']['smtpUsername'])
			->setPassword($config['mail']['smtpPassword']);
	} else
		$transport = Swift_MailTransport::newInstance();

	// Create the Mailer using the created Transport

	$mailer = Swift_Mailer::newInstance($transport);

	// Create a message

	$message = Swift_Message::newInstance()
		->setSubject('Site > Contato')
		->setFrom($config['mail']['senderEmail'])
		->setTo($config['mail']['senderEmail'])
		->setReplyTo(array($email => $name))
		->setBody($body, 'text/html');

	// Send the message

	if (!$mailer->send($message))
		$errors[] = 'Não foi possível enviar a mensagem. Por favor, tente novamente mais tarde.';
}
?>

<form class="contact-form" id="contactForm" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" role="form">
		<?php if ($post && $errors) : ?>

		<div class="alert alert-danger alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
		  <?php foreach ($errors as $error) : ?>
		  <p><?php echo $error; ?></p>
		  <?php endforeach; ?>
		</div>

		<?php elseif ($post) : ?>

		<div class="alert alert-success alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
		  <p>Obrigado pelo contato. Responderemos o mais breve possível.</p>
		</div>

		<?php endif; ?>

		<?php if (!$post || $errors) : ?>

		<?php foreach ($fields as $name => $field) : ?>
		<div class="form-group <?php echo isset($errors[$name]) ? ' has-error' : ''; ?>">
			<label for="planApplicatio<?php echo ucfirst($name); ?>"><?php echo $field['label']; ?></label>
			<?php if (isset($field['options'])) : ?>
			<select class="form-control" id="contact<?php echo ucfirst($name); ?>" name="Contact[<?php echo $name; ?>]">
				<option value=""><?php echo $field['label']; ?></option>	
				<?php foreach ($field['options'] as $value => $label) : ?>				
				<option value="<?php echo $value; ?>"<?php echo $values[$name] == $value ? ' selected' : ''; ?>><?php echo $label; ?></option>	
				<?php endforeach; ?>
			</select>
			<?php else : ?>
			<input type="text" class="form-control" id="contact<?php echo ucfirst($name); ?>" name="Contact[<?php echo $name; ?>]" value="<?php echo $values['name']; ?>" placeholder="<?php echo $field['label']; ?>">
			<?php endif; ?>
		</div>
		<?php endforeach; ?>

		<div class="form-group margin-bottom-remove text-center">
			<button type="submit" class="btn btn-primary">Enviar</button>
		</div>

		<?php endif; ?>
	</div>
</form>