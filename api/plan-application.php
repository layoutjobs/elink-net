<?php

$post    = isset($_POST['PlanApplication']) ? $_POST['PlanApplication'] : false;
$id      = explode('-', isset($_GET['id']) ? $_GET['id'] : null);
$config  = include('config.php');
$plans   = $config['plans'];
$groupId = $id[0];
$planId  = isset($id[1]) ? $id[1] : null;
$group   = isset($plans[$groupId]) ? $plans[$groupId] : null; 
$plan    = isset($group['plans'][$planId]) ? $group['plans'][$planId] : null;
$values  = array();
$errors  = array();

if (!$group)
	die('<div class="text-center" style="padding: 5%;"><h1>Página não encontrada</h1><p>Desculpe, não encontramos a página solicitada.</p></div>');

$fields  = array(
	'plan' => array(
		'label' => 'Plano',
		'required' => false,
		'hidden' => true,
	),
	'name' => array(
		'label' => 'Nome',
		'required' => true,
	),
	'nameAlt' => array(
		'label' => 'Nome Completo ou Razão Social',
		'required' => true,
	),
	'doc' => array(
		'label' => 'CPF',
		'required' => true,
	),
	'docAlt' => array(
		'label' => 'CPF ou CNPJ',
		'required' => true,
	),
	'doc2' => array(
		'label' => 'RG',
		'required' => true,
	),
	'email' => array(
		'label' => 'E-mail',
		'required' => true,
	),
	'birthDate' => array(
		'label' => 'Data de Nascimento',
		'required' => true,
	),
	'phone' => array(
		'label' => 'Telefone',
		'required' => true,
 	),
	'street' => array(
		'label' => 'Endereço Completo',
		'required' => true,
	),
	'zip' => array(
		'label' => 'CEP',
		'required' => true,
	),
	'city' => array(
		'label' => 'Cidade',
		'required' => true,
	),
	'state' => array(
		'label' => 'Estado',
		'required' => true,
		'options' => array(
			'SP' => 'São Paulo',
			'MG' => 'Minas Gerais',
		),
	),
	'installation' => array(
		'label' => 'Instalação',
		'required' => true,
		'options' => $group['installations'],
	),
	'router' => array(
		'label' => 'Roteador',
		'required' => true,
		'options' => $group['routers'],
	),
);

foreach ($fields as $name => $field) {
	if ($name != 'plan' && !in_array($name, $group['applicationFields']))
		unset($fields[$name]);
}

foreach ($fields as $name => $field) {
	$values[$name] = isset($post[$name]) ? $post[$name] : '';
}

if ($post) {
	foreach ($fields as $name => $field) {
		if ($field['required'] && $values[$name] == '')
			$errors[$name] = $field['label'] . ' é obrigatório';
	}
}

// --------------------------------------

if ($post && !$errors) {

	$values['plan'] = $group['name'] . ($plan ? ' / ' . $plan['name'] : '');

	$name = isset($post['nameAlt']) ? $post['nameAlt'] : $post['name'];
	$email = $post['email'];

	$body  = "<div style=\"font: 11pt/1.4 Arial, 'Helvetica', sans-serif\";>";
	$body .= "<p style=\"margin-bottom: 11pt;\">Olá, $name enviou uma mensagem através do site.</p>";
	$body .= "<table style=\"margin-bottom: 11pt;\"><tbody>";
	foreach ($values as $name => $value) {
		$label = $fields[$name]['label'];
		$body .= "<tr><th align=\"left\">$label: </th><td>$value</td>";
	}
	$body .= "</tbody></table></div>";

	// Import Swift library

	require_once 'vendor' . DIRECTORY_SEPARATOR . 'swiftmailer' . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'swift_required.php';

	// Create the Transport

	if ($config['mail']['smtpHost']) {
		$transport = Swift_SmtpTransport::newInstance()
			->setHost($config['mail']['smtpHost'])
			->setPort($config['mail']['smtpPort'])
			->setEncryption($config['mail']['smtpEncryption'])
			->setUsername($config['mail']['smtpUsername'])
			->setPassword($config['mail']['smtpPassword']);
	} else
		$transport = Swift_MailTransport::newInstance();

	// Create the Mailer using the created Transport

	$mailer = Swift_Mailer::newInstance($transport);

	// Create a message

	$message = Swift_Message::newInstance()
		->setSubject('Site > Contratação')
		->setFrom($config['mail']['senderEmail'])
		->setTo($config['mail']['senderEmail'])
		->setReplyTo(array($email => $name))
		->setBody($body, 'text/html');

	// Send the message

	if (!$mailer->send($message))
		$errors[] = 'Não foi possível enviar a mensagem. Por favor, tente novamente mais tarde.';
}
?>

<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" role="form">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
		<?php if ($plan) : ?>
		<h4 class="modal-title text-center">Preencha o formulário abaixo e adquira seu plano <br class="visible-md visible-lg"><strong><?php echo $plan['name']; ?></strong> por apenas <strong><?php echo $plan['price']; ?></strong> mensais.</h4>
		<?php else : ?>
		<h4 class="modal-title text-center">Preencha o formulário abaixo para obter informações <br class="visible-md visible-lg">sobre o plano <strong><?php echo $group['name']; ?></strong>.</h4>
		<?php endif; ?>
	</div>
	<div class="modal-body">
		<?php if ($post && $errors) : ?>

		<div class="alert alert-danger alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
		  <?php foreach ($errors as $error) : ?>
		  <p><?php echo $error; ?></p>
		  <?php endforeach; ?>
		</div>

		<?php elseif ($post) : ?>

		<div class="alert alert-success alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
			<?php if ($plan) : ?>
			<p>Obrigado pelo contato, retornaremos em breve para agendar a data de instalação de seu plano <strong><?php echo $plan['name']; ?></strong>.</p>
			<?php else : ?>
		  <p>Obrigado! Recebemos o sua mensagem. Por favor, aguarde nosso contato.</p>
			<?php endif; ?>
		</div>

		<?php endif; ?>

		<?php if (!$post || $errors) : ?>

		<?php foreach ($fields as $name => $field) : ?>

		<?php if (!isset($field['hidden']) || $field['hidden'] != true) : ?>	

		<div class="form-group <?php echo isset($errors[$name]) ? ' has-error' : ''; ?>">
			<label class="sr-only" for="planApplicatio<?php echo ucfirst($name); ?>"><?php echo $field['label']; ?></label>
			<?php if (isset($field['options'])) : ?>
			<select class="form-control input-sm" id="planApplication<?php echo ucfirst($name); ?>" name="PlanApplication[<?php echo $name; ?>]">
				<option value=""><?php echo $field['label']; ?></option>	
				<?php foreach ($field['options'] as $value => $label) : ?>				
				<option value="<?php echo $value; ?>"<?php echo $values[$name] == $value ? ' selected' : ''; ?>><?php echo $label; ?></option>	
				<?php endforeach; ?>
			</select>
			<?php else : ?>
			<input type="text" class="form-control input-sm" id="planApplication<?php echo ucfirst($name); ?>" name="PlanApplication[<?php echo $name; ?>]" value="<?php echo $values[$name]; ?>" placeholder="<?php echo $field['label']; ?>">
			<?php endif; ?>
		</div>

		<?php endif; ?>

		<?php endforeach; ?>

		<div class="form-group text-center">
			<small><?php echo $group['applicationNotes']; ?></small>
		</div>

		<div class="form-group margin-bottom-remove text-center">
			<button type="submit" class="btn btn-primary">Enviar</button>
		</div>

		<?php endif; ?>
	</div>
</form>