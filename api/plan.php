<?php 

$config = require('config.php');
$plans  = $config['plans'];

if (empty($_GET['id']) || empty($plans[$_GET['id']])) 
	die('<div class="text-center" style="padding: 5%;"><h1>Página não encontrada</h1><p>Desculpe, não encontramos a página solicitada.</p></div>');

$group = $plans[$_GET['id']];
$groupId = $_GET['id'];
?>

<?php if ($group !== null) : ?>

	<?php if ($group['colored']) : ?>

	<table class="table plan-grid-colored">
		<?php foreach ($group['plans'] as $planId => $plan) : ?>
		<tr style="background-color: <?php echo $plan['color']; ?>; color: #fff;">
			<td class="plan-name text-nowrap"><h3><b><?php echo $plan['name']; ?></b></h3></td>
			<td class="plan-description"><?php echo $plan['description']; ?></td>
			<td class="plan-price text-nowrap text-right">
				<h3 class="text-bold"><?php echo $plan['price']; ?></h3>
				<small class="text-uppercase text-muted">mensais</small>
			</td>
			<td class="plan-actions">
				<button class="btn btn-primary text-uppercase" type="button" data-toggle="modal" data-target="#modal" data-remote="api/plan-application.php?id=<?php echo $groupId . '-' . $planId; ?>">Adquira aqui!</button>
			</td>
		</tr>
		<?php endforeach; ?>
	</table>

	<?php else : ?>

	<table class="table">
		<?php foreach ($group['plans'] as $planId => $plan) : ?>
		<tr>
			<td class="plan-name text-nowrap"><h3><b><?php echo $plan['name']; ?></b></h3></td>
			<td class="plan-description"><?php echo $plan['description']; ?></td>
			<td class="plan-price text-nowrap text-right">
				<h3 class="text-bold"><?php echo $plan['price']; ?></h3>
				<small class="text-uppercase text-muted">mensais</small>
			</td>
			<td class="plan-actions">
				<button class="btn btn-primary text-uppercase" type="button" data-toggle="modal" data-target="#modal" data-remote="api/plan-application.php?id=<?php echo $groupId . '-' . $planId; ?>">Adquira aqui!</button>
			</td>
		</tr>
		<?php endforeach; ?>
	</table>

	<?php endif; ?>

	<small><?php echo $group['notes']; ?></small>

<?php endif; ?>
